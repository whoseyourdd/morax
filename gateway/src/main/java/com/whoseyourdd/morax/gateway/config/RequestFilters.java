package com.whoseyourdd.morax.gateway.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;

import reactor.core.publisher.Mono;

@Component
public class RequestFilters implements GlobalFilter, Ordered {

	private static final Logger LOGGER = LoggerFactory.getLogger(RequestFilters.class);

	public static final String AUTHORIZATION_HEADER = "Authorization";
	public static final String PREFIX_TOKEN = "Bearer ";
	public static final String PREFIX_BASIC = "Basic ";

	@Override
	public int getOrder() {
		return Ordered.HIGHEST_PRECEDENCE;
	}

	private String getTokenFromHeader(HttpHeaders headers) throws RuntimeException {
		if (headers.containsKey(AUTHORIZATION_HEADER)) {
			String authHeader = headers.getFirst(AUTHORIZATION_HEADER);
			if (authHeader.startsWith(PREFIX_TOKEN)) {
				LOGGER.info("Incoming request using token");
				return authHeader.substring(PREFIX_TOKEN.length());
			} else if (authHeader.startsWith(PREFIX_BASIC)) {
				LOGGER.info("Incoming request using basic auth");
				return authHeader.substring(PREFIX_BASIC.length());
			} else {
				throw new RuntimeException("Invalid authorization header");
			}
		} return null;
	}

	@Override
	public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
		ServerHttpRequest request = exchange.getRequest();
		HttpHeaders headers = request.getHeaders();
		String token = getTokenFromHeader(headers);

		String userAgent = headers.getFirst(HttpHeaders.USER_AGENT);
		String remoteAddress = request.getRemoteAddress().getAddress().toString();
		String contentType = headers.getFirst(HttpHeaders.CONTENT_TYPE);

		// Log user agent, IP address, and token
		LOGGER.info("User Agent: {}, IP Address: {}, Token: {}, Content Type: {}", userAgent, request.getRemoteAddress(), token, contentType);

		ServerHttpRequest modifiedRequest = exchange.getRequest()
				.mutate()
				.header("X-User-Agent", userAgent)
				.header("X-Remote-Address", remoteAddress)
				.build();

		return chain.filter(exchange.mutate().request(modifiedRequest).build());
	}
}
