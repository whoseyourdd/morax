package com.whoseyourdd.morax.gateway.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;

import reactor.core.publisher.Mono;

@Component
public class ResponseFilters implements GlobalFilter, Ordered {

	private final Logger LOGGER = LoggerFactory.getLogger(ResponseFilters.class);

	@Override
	public int getOrder() {
		return Ordered.LOWEST_PRECEDENCE;
	}

	@Override
	public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
		return chain.filter(exchange).then(Mono.fromRunnable(() -> {
			ServerHttpResponse response = exchange.getResponse();
			HttpStatusCode status = response.getStatusCode();
			LOGGER.info("Service Response Status: {}", response.getStatusCode().value());
			if (status != null && status.is2xxSuccessful()) {
				response.setStatusCode(HttpStatus.OK);
			}
		}));
	}
}
