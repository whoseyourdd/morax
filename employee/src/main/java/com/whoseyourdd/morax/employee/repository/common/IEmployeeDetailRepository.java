package com.whoseyourdd.morax.employee.repository.common;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.whoseyourdd.morax.employee.model.common.EmployeeDetail;

public interface IEmployeeDetailRepository extends JpaRepository<EmployeeDetail, String> {

	Optional<EmployeeDetail> findByEmployeeId(String employeeId);
}
