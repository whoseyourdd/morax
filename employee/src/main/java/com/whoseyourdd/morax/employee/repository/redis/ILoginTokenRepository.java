package com.whoseyourdd.morax.employee.repository.redis;

import com.whoseyourdd.morax.employee.model.redis.LoginToken;
import com.whoseyourdd.morax.employee.utils.error.ErrorUnauthorized;

public interface ILoginTokenRepository {

	LoginToken get(String token) throws ErrorUnauthorized;
}
