package com.whoseyourdd.morax.employee.service;

import com.whoseyourdd.morax.employee.utils.error.ErrorUnauthorized;

import jakarta.servlet.http.HttpServletRequest;

public interface IAuthorizationService {
	boolean authorizeToken(HttpServletRequest request) throws ErrorUnauthorized;

	boolean isAdmin(HttpServletRequest request) throws ErrorUnauthorized;
}
