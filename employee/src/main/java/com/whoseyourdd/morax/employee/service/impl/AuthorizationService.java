package com.whoseyourdd.morax.employee.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.whoseyourdd.morax.employee.model.redis.LoginToken;
import com.whoseyourdd.morax.employee.repository.redis.ILoginTokenRepository;
import com.whoseyourdd.morax.employee.service.IAuthorizationService;
import com.whoseyourdd.morax.employee.utils.constant.MyConstant;
import com.whoseyourdd.morax.employee.utils.date.DateUtils;
import com.whoseyourdd.morax.employee.utils.error.ErrorUnauthorized;

import jakarta.servlet.http.HttpServletRequest;

@Service
public class AuthorizationService implements IAuthorizationService {

	@Autowired
	ILoginTokenRepository loginTokenRepository;

	@Override
	public boolean authorizeToken(HttpServletRequest request) throws ErrorUnauthorized {
		return getLoginToken(request) != null;
	}

	@Override
	public boolean isAdmin(HttpServletRequest request) throws ErrorUnauthorized {
		LoginToken loginToken = getLoginToken(request);
		if (!loginToken.getRole().equalsIgnoreCase("admin")) {
			throw new ErrorUnauthorized("You don't have permission to do this action");
		}
		return true;
	}

	private LoginToken getLoginToken(HttpServletRequest request) throws ErrorUnauthorized {
		String authorization = request.getHeader(MyConstant.AUTH_HEADER);
		if (authorization == null || !authorization.startsWith(MyConstant.AUTH_HEADER_PREFIX)) {
			throw new ErrorUnauthorized("Invalid Authorization Header");
		}
		String[] parts = authorization.split(" ");
		if (parts.length != 2) {
			throw new ErrorUnauthorized("Invalid Authorization Format");
		}
		String token = parts[1];
		LoginToken loginToken = (LoginToken) loginTokenRepository.get(token);
		if (loginToken == null) {
			throw new ErrorUnauthorized("Invalid Token");
		}
		if (!DateUtils.isDateGreaterThanToday(loginToken.getTtl())) {
			throw new ErrorUnauthorized("Token already expired");
		}
		return loginToken;
	}

}
