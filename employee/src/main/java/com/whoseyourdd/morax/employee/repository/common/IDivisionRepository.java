package com.whoseyourdd.morax.employee.repository.common;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.whoseyourdd.morax.employee.model.common.Division;

public interface IDivisionRepository extends JpaRepository<Division, String> {

    @Query(value = "SELECT d.* FROM divisions d WHERE d.name = :name", nativeQuery = true)
    Division findByName(@Param("name") String name);

    @Query(value = "SELECT * FROM divisions ORDER BY name ASC", nativeQuery = true)
    List<Division> findAllByOrderNameAsc();
}
