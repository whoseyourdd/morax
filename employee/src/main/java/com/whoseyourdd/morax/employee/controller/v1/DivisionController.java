package com.whoseyourdd.morax.employee.controller.v1;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.whoseyourdd.morax.employee.dto.DivisionDTO;
import com.whoseyourdd.morax.employee.dto.EmployeeDTO;
import com.whoseyourdd.morax.employee.service.IAuthorizationService;
import com.whoseyourdd.morax.employee.service.IDivisionService;
import com.whoseyourdd.morax.employee.service.IEmployeeService;
import com.whoseyourdd.morax.employee.utils.mapper.ErrorResponseBuilder;
import com.whoseyourdd.morax.employee.utils.mapper.ResponseBuilder;

import jakarta.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/v1/divisions")
public class DivisionController {

	@Autowired
	private IEmployeeService employeeService;

	@Autowired
	private IDivisionService divisionService;

	@Autowired
	private ErrorResponseBuilder errorBuilder;

	@Autowired
	private ResponseBuilder responseBuilder;

	@Autowired
	private IAuthorizationService authorizationService;

	@GetMapping("/list-employee")
	@Cacheable(value = "employee", key = "#divisionName")
	public ResponseEntity<?> getEmployeeWhereDivisionNameLike(@RequestParam("division") String divisionName, HttpServletRequest request) {
		Long startTime = System.currentTimeMillis();
		try {
			authorizationService.authorizeToken(request);
			List<EmployeeDTO> employees = employeeService.findEmployeeWhereByDivisionNameLike(divisionName);
			return responseBuilder.buildSuccessResponse(employees, HttpStatus.OK, "Employees retrieved successfully",
					startTime);
		} catch (Exception e) {
			return errorBuilder.handleException(e, startTime);
		}
	}

	@PostMapping("/create")
	public ResponseEntity<?> createDivision(@RequestParam("division") String divisionName, HttpServletRequest request) {
		Long startTime = System.currentTimeMillis();
		try {
			authorizationService.isAdmin(request);
			divisionService.createDivision(divisionName);
			return responseBuilder.buildSuccessResponse(null, HttpStatus.CREATED, "Division created successfully",
					startTime);
		} catch (Exception e) {
			return errorBuilder.handleException(e, startTime);
		}
	}

	@GetMapping
	@Cacheable(value = "division", key = "#divisionName")
	public ResponseEntity<?> getAllDivisions(HttpServletRequest request) {
		Long startTime = System.currentTimeMillis();
		try {
			authorizationService.authorizeToken(request);
			List<DivisionDTO> divisions = divisionService.findAllByOrderNameAsc();
			return responseBuilder.buildSuccessResponse(divisions, HttpStatus.OK, "Divisions retrieved successfully",
					startTime);
		} catch (Exception e) {
			return errorBuilder.handleException(e, startTime);
		}
	}

	@GetMapping("/get-name")
	@Cacheable(value = "division", key = "#divisionName")
	public ResponseEntity<?> getDivisionByName(@RequestParam("division") String divisionName, HttpServletRequest request) {
		Long startTime = System.currentTimeMillis();
		try {
			authorizationService.authorizeToken(request);
			DivisionDTO division = divisionService.findByName(divisionName);
			return responseBuilder.buildSuccessResponse(division, HttpStatus.OK, "Division retrieved successfully",
					startTime);
		} catch (Exception e) {
			return errorBuilder.handleException(e, startTime);
		}
	}
}
