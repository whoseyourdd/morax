package com.whoseyourdd.morax.employee.utils.mapper;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.whoseyourdd.morax.employee.dto.ErrorResponseDTO;
import com.whoseyourdd.morax.employee.utils.date.DateUtils;
import com.whoseyourdd.morax.employee.utils.date.TimeUtils;
import com.whoseyourdd.morax.employee.utils.error.ErrorAlreadyExist;
import com.whoseyourdd.morax.employee.utils.error.ErrorInvalidRequest;
import com.whoseyourdd.morax.employee.utils.error.ErrorNotFound;
import com.whoseyourdd.morax.employee.utils.error.ErrorUnauthorized;

@Component
public class ErrorResponseBuilder {
	public ResponseEntity<?> handleException(Exception e, Long startTime) {
		int statusCode = HttpStatus.INTERNAL_SERVER_ERROR.value();
		if (e instanceof ErrorAlreadyExist) {
			statusCode = HttpStatus.CONFLICT.value();
		} else if (e instanceof ErrorInvalidRequest) {
			statusCode = HttpStatus.BAD_REQUEST.value();
		} else if (e instanceof ErrorUnauthorized) {
			statusCode = HttpStatus.UNAUTHORIZED.value();
		} else if (e instanceof ErrorNotFound) {
			statusCode = HttpStatus.NOT_FOUND.value();
		}
		Long duration = System.currentTimeMillis() - startTime;
		ErrorResponseDTO responseError = ErrorResponseDTO.builder()
				.statusCode(statusCode)
				.error(e.getMessage())
				.duration(TimeUtils.durationFormatter(duration))
				.time(DateUtils.formatDate(new Date()))
				.build();
		return ResponseEntity.status(statusCode).body(responseError);
	}
}
