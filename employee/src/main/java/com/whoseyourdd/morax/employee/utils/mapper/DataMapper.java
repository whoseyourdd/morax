package com.whoseyourdd.morax.employee.utils.mapper;

import org.springframework.stereotype.Component;

import com.whoseyourdd.morax.employee.dto.BranchDTO;
import com.whoseyourdd.morax.employee.dto.DivisionDTO;
import com.whoseyourdd.morax.employee.dto.EmployeeDTO;
import com.whoseyourdd.morax.employee.dto.RequestCreateEmployeeDTO;
import com.whoseyourdd.morax.employee.dto.RequestUpdateEmployeeDTO;
import com.whoseyourdd.morax.employee.model.common.Branch;
import com.whoseyourdd.morax.employee.model.common.Division;
import com.whoseyourdd.morax.employee.model.common.Employee;
import com.whoseyourdd.morax.employee.model.common.EmployeeDetail;
import com.whoseyourdd.morax.employee.utils.date.DateUtils;

@Component
public class DataMapper {

	public EmployeeDTO getEmployee(Employee employee, EmployeeDetail employeeDetail, Branch branch, Division divisions) {
		BranchDTO branchDTO = BranchDTO.builder()
				.branchId(branch.getId())
				.branchName(branch.getName())
				.build();

		DivisionDTO divisionDTO = DivisionDTO.builder()
				.divisionId(divisions.getId())
				.divisionName(divisions.getName())
				.build();

		EmployeeDTO employeeDTO = EmployeeDTO.builder()
				.id(employee.getId())
				.name(employee.getName())
				.email(employee.getEmail())
				.citizenshipId(employeeDetail.getCitizenshipId())
				.division(divisionDTO)
				.branch(branchDTO)
				.joinDate(DateUtils.formatDate(employeeDetail.getJoinDate()))
				.leaveDate(DateUtils.formatDate(employeeDetail.getLeaveDate()))
				.position(employeeDetail.getPosition())
				.isActive(employee.getIsActive())
				.build();

		return employeeDTO;
	}

	public Employee createEmployee(RequestCreateEmployeeDTO requestCreateEmployeeDTO) {
		return Employee.builder()
				.name(requestCreateEmployeeDTO.getName())
				.email(requestCreateEmployeeDTO.getEmail())
				.build();
	}

	public EmployeeDetail createEmployeeDetail(RequestCreateEmployeeDTO requestCreateEmployeeDTO) {
		return EmployeeDetail.builder()
				.citizenshipId(requestCreateEmployeeDTO.getCitizenshipId())
				.divisionId(requestCreateEmployeeDTO.getDivisionId())
				.branchId(requestCreateEmployeeDTO.getBranchId())
				.joinDate(DateUtils.parseDate(requestCreateEmployeeDTO.getJoinDate()))
				.leaveDate(DateUtils.parseDate(requestCreateEmployeeDTO.getLeaveDate()))
				.position(requestCreateEmployeeDTO.getPosition())
				.build();
	}

	public Employee updateEmployee(RequestUpdateEmployeeDTO requestUpdateEmployeeDTO) {
		return Employee.builder()
				.name(requestUpdateEmployeeDTO.getName())
				.build();
	}

	public EmployeeDetail updateEmployeeDetail(RequestUpdateEmployeeDTO requestUpdateEmployeeDTO) {
		return EmployeeDetail.builder()
				.citizenshipId(requestUpdateEmployeeDTO.getCitizenshipId())
				.divisionId(requestUpdateEmployeeDTO.getDivisionId())
				.branchId(requestUpdateEmployeeDTO.getBranchId())
				.joinDate(DateUtils.parseDate(requestUpdateEmployeeDTO.getJoinDate()))
				.leaveDate(DateUtils.parseDate(requestUpdateEmployeeDTO.getLeaveDate()))
				.position(requestUpdateEmployeeDTO.getPosition())
				.build();
	}
}
