package com.whoseyourdd.morax.employee.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DivisionDTO {
	@JsonProperty("division_id")
	private String divisionId;

	@JsonProperty("division_name")
	private String divisionName;

}
