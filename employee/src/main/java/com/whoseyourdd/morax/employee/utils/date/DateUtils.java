package com.whoseyourdd.morax.employee.utils.date;

import java.util.Date;

public class DateUtils {

	public static String formatDate(Date date) {
		java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			return sdf.format(date);
		} catch (Exception e) {
			return null;
		}
	}

	public static Date parseDate(String date) {
		java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			return sdf.parse(date);
		} catch (Exception e) {
			return null;
		}
	}

	public static boolean isDateGreaterThanToday(String strDate) {
		Date date = parseDate(strDate);
		try {
			return date.after(new Date());
		} catch (Exception e) {
			return false;
		}
	}
}
