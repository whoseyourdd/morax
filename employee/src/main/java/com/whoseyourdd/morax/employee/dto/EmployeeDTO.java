package com.whoseyourdd.morax.employee.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class EmployeeDTO {
	@JsonProperty("employee_id")
	private String id;

	@JsonProperty("employee_name")
	private String name;

	@JsonProperty("employee_email")
	private String email;

	@JsonProperty("citizenship_id")
	private String citizenshipId;

	@JsonProperty("division")
	private DivisionDTO division;

	@JsonProperty("branch")
	private BranchDTO branch;

	@JsonProperty("branch_id")
	private String branchId;

	@JsonProperty("position")
	private String position;

	@JsonProperty("join_date")
	private String joinDate;

	@JsonProperty("leave_date")
	private String leaveDate;

	@JsonProperty("is_active")
	private Boolean isActive;
}
