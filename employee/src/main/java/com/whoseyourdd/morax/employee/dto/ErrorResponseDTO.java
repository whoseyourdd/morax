package com.whoseyourdd.morax.employee.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ErrorResponseDTO {
	@JsonProperty("status_code")
	private int statusCode;

	@JsonProperty("error")
	private String error;

	@JsonProperty("time")
	private String time;

	@JsonProperty("duration")
	private String duration;
}
