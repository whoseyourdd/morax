package com.whoseyourdd.morax.employee.service;

import java.util.List;

import com.whoseyourdd.morax.employee.dto.EmployeeDTO;
import com.whoseyourdd.morax.employee.dto.RequestCreateEmployeeDTO;
import com.whoseyourdd.morax.employee.dto.RequestUpdateEmployeeDTO;
import com.whoseyourdd.morax.employee.utils.error.ErrorAlreadyExist;
import com.whoseyourdd.morax.employee.utils.error.ErrorInvalidRequest;
import com.whoseyourdd.morax.employee.utils.error.ErrorNotFound;

public interface IEmployeeService {
	EmployeeDTO createEmployee(RequestCreateEmployeeDTO requestCreateEmployeeDTO)
			throws ErrorAlreadyExist, ErrorNotFound, ErrorInvalidRequest;

	List<EmployeeDTO> getAllEmployees();

	EmployeeDTO getEmployeeById(String id) throws ErrorNotFound;

	EmployeeDTO updateEmployee(String id, RequestUpdateEmployeeDTO requestUpdateEmployeeDTO)
			throws ErrorNotFound, ErrorInvalidRequest;

	void deleteEmployee(String id) throws ErrorNotFound;

	List<EmployeeDTO> findByNameLike(String name);

	List<EmployeeDTO> findEmployeeThatAboutToLeave();

	List<EmployeeDTO> findEmployeeWhereByDivisionNameLike(String divisionName);

	List<EmployeeDTO> findEmployeeWhereByBranchNameLike(String branchName);
}
