package com.whoseyourdd.morax.employee.model.common;

import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "employee_details")
public class EmployeeDetail {
	@Id
	@Column(name = "id", nullable = false, unique = true)
	private String id;

	@Column(name = "employee_id", nullable = false, unique = true)
	private String employeeId;

	@Column(name = "citizenship_id", nullable = false, unique = true)
	private String citizenshipId;

	@Column(name = "division_id", nullable = false, unique = true)
	private String divisionId;

	@Column(name = "branch_id", nullable = false, unique = true)
	private String branchId;

	@Column(name = "position", nullable = false)
	private String position;

	@Column(name = "join_date", nullable = false)
	private Date joinDate;

	@Column(name = "leave_date")
	private Date leaveDate;

	@Column(name = "created_at", nullable = false)
	private Date createdAt;

	@Column(name = "updated_at")
	private Date updatedAt;
}
