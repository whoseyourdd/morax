package com.whoseyourdd.morax.employee.utils.error;

public class ErrorNotFound extends Exception {

	public ErrorNotFound(String message) {
		super(message);
	}
}
