package com.whoseyourdd.morax.employee.service;

import java.util.List;

import com.whoseyourdd.morax.employee.dto.BranchDTO;
import com.whoseyourdd.morax.employee.utils.error.ErrorAlreadyExist;
import com.whoseyourdd.morax.employee.utils.error.ErrorNotFound;

public interface IBranchService {

	void create(String branchName) throws ErrorAlreadyExist;

	BranchDTO findByName(String name) throws ErrorNotFound;

	List<BranchDTO> findAll();

	BranchDTO findById(String id) throws ErrorNotFound;

}
