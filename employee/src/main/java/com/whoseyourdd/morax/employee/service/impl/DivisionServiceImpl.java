package com.whoseyourdd.morax.employee.service.impl;

import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.whoseyourdd.morax.employee.dto.DivisionDTO;
import com.whoseyourdd.morax.employee.model.common.Division;
import com.whoseyourdd.morax.employee.repository.common.IDivisionRepository;
import com.whoseyourdd.morax.employee.service.IDivisionService;
import com.whoseyourdd.morax.employee.utils.error.ErrorAlreadyExist;
import com.whoseyourdd.morax.employee.utils.error.ErrorNotFound;

@Service
public class DivisionServiceImpl implements IDivisionService {

	@Autowired
	private IDivisionRepository divisionRepository;

	@Override
	public void createDivision(String name) throws ErrorAlreadyExist {
		try {
			String uid = UUID.randomUUID().toString();
			Division division = Division.builder()
					.id(uid)
					.name(name)
					.createdAt(new Date())
					.isActive(true)
					.build();
			divisionRepository.save(division);
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public DivisionDTO findByName(String name) throws ErrorNotFound {
		try {
			Division division = divisionRepository.findByName(name);
			if (division != null) {
				return DivisionDTO.builder()
						.divisionId(division.getId())
						.divisionName(division.getName())
						.build();
			} else {
				throw new ErrorNotFound("Data with this name is not found");
			}
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public List<DivisionDTO> findAllByOrderNameAsc() {
		try {
			List<Division> divisions = divisionRepository.findAllByOrderNameAsc();
			return divisions.stream()
					.map(division -> {
						return DivisionDTO.builder()
								.divisionId(division.getId())
								.divisionName(division.getName())
								.build();
					}).collect(Collectors.toList());
		} catch (Exception e) {
			throw e;
		}
	}

}
