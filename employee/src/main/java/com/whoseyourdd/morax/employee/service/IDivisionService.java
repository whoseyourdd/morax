package com.whoseyourdd.morax.employee.service;

import java.util.List;

import com.whoseyourdd.morax.employee.dto.DivisionDTO;
import com.whoseyourdd.morax.employee.utils.error.ErrorAlreadyExist;
import com.whoseyourdd.morax.employee.utils.error.ErrorNotFound;

public interface IDivisionService {

	void createDivision(String name) throws ErrorAlreadyExist;

	DivisionDTO findByName(String name) throws ErrorNotFound;

	List<DivisionDTO> findAllByOrderNameAsc();
}
