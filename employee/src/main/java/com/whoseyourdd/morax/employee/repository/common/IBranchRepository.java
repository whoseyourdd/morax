package com.whoseyourdd.morax.employee.repository.common;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.whoseyourdd.morax.employee.model.common.Branch;

public interface IBranchRepository extends JpaRepository<Branch, String> {

    @Query(value = "SELECT * FROM branches WHERE name = :name", nativeQuery = true)
    Branch findByName(@Param("name") String name);
}
