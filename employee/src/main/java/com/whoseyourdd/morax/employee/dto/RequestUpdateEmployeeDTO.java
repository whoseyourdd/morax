package com.whoseyourdd.morax.employee.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RequestUpdateEmployeeDTO {
	@JsonProperty("employee_name")
	private String name;

	@JsonProperty("citizenship_id")
	private String citizenshipId;

	@JsonProperty("division_id")
	private String divisionId;

	@JsonProperty("branch_id")
	private String branchId;

	@JsonProperty("position")
	private String position;

	@JsonProperty("join_date")
	private String joinDate;

	@JsonProperty("leave_date")
	private String leaveDate;
}
