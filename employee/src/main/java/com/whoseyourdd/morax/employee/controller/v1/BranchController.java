package com.whoseyourdd.morax.employee.controller.v1;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.whoseyourdd.morax.employee.dto.BranchDTO;
import com.whoseyourdd.morax.employee.service.IAuthorizationService;
import com.whoseyourdd.morax.employee.service.IBranchService;
import com.whoseyourdd.morax.employee.utils.mapper.ErrorResponseBuilder;
import com.whoseyourdd.morax.employee.utils.mapper.ResponseBuilder;

import jakarta.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/api/v1/branch")
public class BranchController {
    
    @Autowired
    private IBranchService branchService;

    @Autowired
	private ErrorResponseBuilder errorBuilder;

	@Autowired
	private ResponseBuilder responseBuilder;

	@Autowired
	private IAuthorizationService authorizationService;

    @PostMapping("/create")
    public ResponseEntity<?> createBranch(@RequestParam("branch") String branchName, HttpServletRequest request) {
        Long startTime = System.currentTimeMillis();
        try {
            authorizationService.isAdmin(request);
            branchService.create(branchName);
            return responseBuilder.buildSuccessResponse(null, HttpStatus.CREATED, "Branch created successfully",
                    startTime);
        } catch (Exception e) {
            return errorBuilder.handleException(e, startTime);
        }
    }

    @GetMapping("/get-name")
    @Cacheable(value = "branch", key = "#branchName")
    public ResponseEntity<?> getBranchByName(@RequestParam("name") String branchName, HttpServletRequest request) {
        Long startTime = System.currentTimeMillis();
        try {
            authorizationService.authorizeToken(request);
            BranchDTO branch = branchService.findByName(branchName);
            return responseBuilder.buildSuccessResponse(branch, HttpStatus.OK, "Branch retrieved successfully",
                    startTime);
        } catch (Exception e) {
            return errorBuilder.handleException(e, startTime);
        }
    }

    @GetMapping
    public ResponseEntity<?> getAll(HttpServletRequest request) {
        Long startTime = System.currentTimeMillis();
        try {
            authorizationService.authorizeToken(request);
            List<BranchDTO> branch = branchService.findAll();
            return responseBuilder.buildSuccessResponse(branch, HttpStatus.OK, "Branches retrieved successfully",
                    startTime);
        } catch (Exception e) {
            return errorBuilder.handleException(e, startTime);
        }
    }

    @GetMapping("/get-id")
    @Cacheable(value = "branch", key = "#id")
    public ResponseEntity<?> getBranchById(@RequestParam("id") String id, HttpServletRequest request) {
        Long startTime = System.currentTimeMillis();
        try {
            authorizationService.authorizeToken(request);
            BranchDTO branch = branchService.findById(id);
            return responseBuilder.buildSuccessResponse(branch, HttpStatus.OK, "Branch retrieved successfully",
                    startTime);
        } catch (Exception e) {
            return errorBuilder.handleException(e, startTime);
        }
    }

}
