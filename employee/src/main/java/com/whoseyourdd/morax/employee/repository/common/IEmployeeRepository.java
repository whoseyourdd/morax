package com.whoseyourdd.morax.employee.repository.common;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.whoseyourdd.morax.employee.model.common.Employee;
import java.util.List;

public interface IEmployeeRepository extends JpaRepository<Employee, String> {

	@Query(value = "SELECT * FROM employees WHERE email = :email", nativeQuery = true)
	Optional<Employee> findByEmail(@Param("email") String email);

	@Query(value = "SELECT * FROM employees WHERE name LIKE %:name%", nativeQuery = true)
	List<Employee> findByNameLike(@Param("name") String name);

	@Query(value = "SELECT e.* " +
			"FROM employees e " +
			"JOIN employee_details ed ON e.id = ed.employee_id " +
			"JOIN divisions d ON ed.division_id = d.id " +
			"WHERE d.name LIKE %:division_name%", nativeQuery = true)
	List<Employee> findByDivisionWhereDivisionNameLike(@Param("division_name") String pattern);

	@Query(value = "SELECT e.* " +
			"FROM employees e " +
			"JOIN employee_details ed ON e.id = ed.employee_id " +
			"JOIN branches b ON ed.branch_id = b.id " +
			"WHERE b.name LIKE %:branch_name%", nativeQuery = true)
	List<Employee> findByBranchWhereBranchNameLike(@Param("branch_name") String pattern);

	@Query(value = "SELECT e.* " +
			"FROM employees e " +
			"JOIN employee_details ed ON e.id = ed.employee_id " +
			"WHERE ed.leave_date BETWEEN CURRENT_DATE() AND DATE_ADD(CURRENT_DATE(), INTERVAL 30 DAY)", nativeQuery = true)
	List<Employee> findEmployeeThatAboutToLeave();
}
