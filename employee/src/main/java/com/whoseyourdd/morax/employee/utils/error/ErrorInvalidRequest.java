package com.whoseyourdd.morax.employee.utils.error;

public class ErrorInvalidRequest extends Exception {

	public ErrorInvalidRequest(String message) {
		super(message);
	}
}
