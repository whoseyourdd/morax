package com.whoseyourdd.morax.employee.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BranchDTO {
	@JsonProperty("branch_id")
	private String branchId;

	@JsonProperty("branch_name")
	private String branchName;
}
