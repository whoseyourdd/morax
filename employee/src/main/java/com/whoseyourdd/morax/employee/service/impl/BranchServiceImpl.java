package com.whoseyourdd.morax.employee.service.impl;

import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.whoseyourdd.morax.employee.dto.BranchDTO;
import com.whoseyourdd.morax.employee.model.common.Branch;
import com.whoseyourdd.morax.employee.repository.common.IBranchRepository;
import com.whoseyourdd.morax.employee.service.IBranchService;
import com.whoseyourdd.morax.employee.utils.error.ErrorAlreadyExist;
import com.whoseyourdd.morax.employee.utils.error.ErrorNotFound;

@Service
public class BranchServiceImpl implements IBranchService {

	@Autowired
	private IBranchRepository branchRepository;

	@Override
	public void create(String branchName) throws ErrorAlreadyExist {
		try {
			String uid = UUID.randomUUID().toString();
			Branch branch = Branch.builder()
					.id(uid)
					.name(branchName)
					.createdAt(new Date())
					.isActive(true)
					.build();
			branchRepository.save(branch);
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public BranchDTO findByName(String name) throws ErrorNotFound {
		try {
			Branch branch = branchRepository.findByName(name);
			if (branch != null) {
				return BranchDTO.builder()
						.branchId(branch.getId())
						.branchName(branch.getName())
						.build();
			} else {
				throw new ErrorNotFound("Data with this name is not found");
			}
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public List<BranchDTO> findAll() {
		try {
			List<Branch> branches = branchRepository.findAll();
			return branches.stream()
					.map(branch -> {
						return BranchDTO.builder()
								.branchId(branch.getId())
								.branchName(branch.getName())
								.build();
					})
					.collect(Collectors.toList());
		} catch (Exception e) {
			throw e;
		}

	}

	@Override
	public BranchDTO findById(String id) throws ErrorNotFound {
		try {
			Branch branch = branchRepository.findById(id).orElse(null);
			if (branch != null) {
				return BranchDTO.builder()
						.branchId(branch.getId())
						.branchName(branch.getName())
						.build();
			} else {
				throw new ErrorNotFound("Data with this name is not found");
			}
		} catch (Exception e) {
			throw e;
		}
	}

}
