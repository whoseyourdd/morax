package com.whoseyourdd.morax.employee.repository.redis.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import com.google.gson.Gson;
import com.whoseyourdd.morax.employee.model.redis.LoginToken;
import com.whoseyourdd.morax.employee.repository.redis.ILoginTokenRepository;
import com.whoseyourdd.morax.employee.utils.constant.MyConstant;
import com.whoseyourdd.morax.employee.utils.error.ErrorUnauthorized;

@Repository
public class LoginTokenRepositoryImpl implements ILoginTokenRepository {

	@Autowired
	RedisTemplate<String, Object> redisTemplate;

	@Override
	public LoginToken get(String token) throws ErrorUnauthorized {
		try{
			Object object = redisTemplate.opsForHash().get(MyConstant.LOGIN_TOKEN_KEY, token);
			Gson gson = new Gson();
			LoginToken loginToken = gson.fromJson(object.toString(), LoginToken.class);
			return loginToken;
		}catch(Exception e){
			throw new ErrorUnauthorized("Token already expired");
		}
	}
}
