package com.whoseyourdd.morax.employee.utils.error;

public class ErrorUnauthorized extends Exception {

	public ErrorUnauthorized(String message) {
		super(message);
	}
}
