package com.whoseyourdd.morax.employee.controller.v1;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.whoseyourdd.morax.employee.dto.EmployeeDTO;
import com.whoseyourdd.morax.employee.dto.RequestCreateEmployeeDTO;
import com.whoseyourdd.morax.employee.dto.RequestUpdateEmployeeDTO;
import com.whoseyourdd.morax.employee.service.IAuthorizationService;
import com.whoseyourdd.morax.employee.service.IEmployeeService;
import com.whoseyourdd.morax.employee.utils.mapper.ErrorResponseBuilder;
import com.whoseyourdd.morax.employee.utils.mapper.ResponseBuilder;

import jakarta.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/v1/employees")
public class EmployeeController {

	@Autowired
	private IEmployeeService employeeService;

	@Autowired
	private ErrorResponseBuilder errorBuilder;

	@Autowired
	private ResponseBuilder responseBuilder;

	@Autowired
	private IAuthorizationService authorizationService;

	/**
	 * A description of the entire Java function.
	 *
	 * @param requestCreateEmployeeDTO description of parameter
	 * @return description of return value
	 */
	@PostMapping
	public ResponseEntity<?> createEmployee(@RequestBody RequestCreateEmployeeDTO requestCreateEmployeeDTO, HttpServletRequest request) {
		Long startTime = System.currentTimeMillis();
		try {
			authorizationService.isAdmin(request);
			EmployeeDTO createdEmployee = employeeService.createEmployee(requestCreateEmployeeDTO);
			return responseBuilder.buildSuccessResponse(createdEmployee, HttpStatus.CREATED, "Employee created successfully",
					startTime);
				} catch (Exception e) {
					return errorBuilder.handleException(e, startTime);
				}
	}

	/**
	 * Get all employees using a GET request
	 *
	 * @return ResponseEntity with data for all employees
	 */
	@GetMapping
	@Cacheable(value = "employee", key = "#employees")
	public ResponseEntity<?> getAllEmployees(HttpServletRequest request) {
		Long startTime = System.currentTimeMillis();
		try {
			authorizationService.authorizeToken(request);
			List<EmployeeDTO> employees = employeeService.getAllEmployees();
			return responseBuilder.buildSuccessResponse(employees, HttpStatus.OK, "Employees retrieved successfully",
					startTime);
				} catch (Exception e) {
					return errorBuilder.handleException(e, startTime);
				}
	}

	/**
	 * Retrieves an employee by ID.
	 *
	 * @param id The ID of the employee to retrieve
	 * @return A ResponseEntity containing the employee information or an error
	 *         response
	 */
	@GetMapping("/{id}")
	@Cacheable(value = "employee", key = "#id")
	public ResponseEntity<?> getEmployeeById(@PathVariable String id, HttpServletRequest request) {
		Long startTime = System.currentTimeMillis();
		try {
			authorizationService.authorizeToken(request);
			EmployeeDTO employee = employeeService.getEmployeeById(id);
			return responseBuilder.buildSuccessResponse(employee, HttpStatus.OK, "Employee retrieved successfully",
					startTime);
				} catch (Exception e) {
					return errorBuilder.handleException(e, startTime);
				}
	}

	/**
	 * Update an employee with the given request data.
	 *
	 * @param requestUpdateEmployeeDTO the request data for updating the employee
	 * @param id                       the ID of the employee to update
	 * @return the response entity with the updated employee or an error message
	 */
	@PutMapping("/{id}")
	public ResponseEntity<?> updateEmployee(@RequestBody RequestUpdateEmployeeDTO requestUpdateEmployeeDTO,
			@PathVariable String id, HttpServletRequest request) {
		Long startTime = System.currentTimeMillis();
		try {
			authorizationService.isAdmin(request);
			EmployeeDTO updatedEmployee = employeeService.updateEmployee(id, requestUpdateEmployeeDTO);
			return responseBuilder.buildSuccessResponse(updatedEmployee, HttpStatus.OK, "Employee updated successfully",
					startTime);
				} catch (Exception e) {
					e.printStackTrace();
					return errorBuilder.handleException(e, startTime);
				}
	}

	/**
	 * Delete an employee by ID.
	 *
	 * @param id the ID of the employee to be deleted
	 * @return a ResponseEntity indicating the result of the deletion
	 */
	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteEmployee(@PathVariable String id, HttpServletRequest request) {
		Long startTime = System.currentTimeMillis();
		try {
			authorizationService.isAdmin(request);
			employeeService.deleteEmployee(id);
			return responseBuilder.buildSuccessResponse(null, HttpStatus.OK, "Employee deleted successfully", startTime);
		} catch (Exception e) {
			return errorBuilder.handleException(e, startTime);
		}
	}

}
