package com.whoseyourdd.morax.employee.service.impl;

import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.whoseyourdd.morax.employee.dto.EmployeeDTO;
import com.whoseyourdd.morax.employee.dto.RequestCreateEmployeeDTO;
import com.whoseyourdd.morax.employee.dto.RequestUpdateEmployeeDTO;
import com.whoseyourdd.morax.employee.model.common.Branch;
import com.whoseyourdd.morax.employee.model.common.Division;
import com.whoseyourdd.morax.employee.model.common.Employee;
import com.whoseyourdd.morax.employee.model.common.EmployeeDetail;
import com.whoseyourdd.morax.employee.repository.common.IBranchRepository;
import com.whoseyourdd.morax.employee.repository.common.IDivisionRepository;
import com.whoseyourdd.morax.employee.repository.common.IEmployeeDetailRepository;
import com.whoseyourdd.morax.employee.repository.common.IEmployeeRepository;
import com.whoseyourdd.morax.employee.service.IEmployeeService;
import com.whoseyourdd.morax.employee.utils.date.DateUtils;
import com.whoseyourdd.morax.employee.utils.error.ErrorAlreadyExist;
import com.whoseyourdd.morax.employee.utils.error.ErrorInvalidRequest;
import com.whoseyourdd.morax.employee.utils.error.ErrorNotFound;
import com.whoseyourdd.morax.employee.utils.mapper.DataMapper;

@Service
public class EmployeeServiceImpl implements IEmployeeService {

	@Autowired
	DataMapper dataMapper;

	@Autowired
	IEmployeeRepository employeeRepository;

	@Autowired
	IBranchRepository branchRepository;

	@Autowired
	IDivisionRepository divisionRepository;

	@Autowired
	IEmployeeDetailRepository employeeDetailRepository;

	@Override
	public EmployeeDTO createEmployee(RequestCreateEmployeeDTO requestCreateEmployeeDTO)
			throws ErrorAlreadyExist, ErrorNotFound, ErrorInvalidRequest {
		try {
			Employee employee = employeeRepository.findByEmail(requestCreateEmployeeDTO.getEmail()).orElse(null);
			Branch branch = branchRepository.findById(requestCreateEmployeeDTO.getBranchId()).orElse(null);
			Division divisions = divisionRepository.findById(requestCreateEmployeeDTO.getDivisionId()).orElse(null);
			if (branch == null) {
				throw new ErrorNotFound("Data with this branch id is not found");
			}
			if (divisions == null) {
				throw new ErrorNotFound("Data with this division id is not found");
			}
			if (employee != null) {
				throw new ErrorAlreadyExist("Data with this email address is already exist");
			}
			String employeeId = UUID.randomUUID().toString();
			String employeeDetailId = UUID.randomUUID().toString();
			employee = dataMapper.createEmployee(requestCreateEmployeeDTO);
			employee.setId(employeeId);
			employee.setIsActive(false);
			employee.setCreatedAt(new Date());

			EmployeeDetail employeeDetail = dataMapper.createEmployeeDetail(requestCreateEmployeeDTO);
			employeeDetail.setId(employeeDetailId);
			employeeDetail.setEmployeeId(employeeId);
			employeeDetail.setCreatedAt(new Date());

			employeeRepository.save(employee);
			employeeDetailRepository.save(employeeDetail);

			return dataMapper.getEmployee(employee, employeeDetail, branch, divisions);
		} catch (Exception e) {
			throw new ErrorInvalidRequest(e.getLocalizedMessage());
		}
	}

	@Override
	public List<EmployeeDTO> getAllEmployees() {
		List<Employee> employees = employeeRepository.findAll();
		return employees.stream()
				.map(employee -> {
					EmployeeDetail employeeDetail = employeeDetailRepository.findByEmployeeId(employee.getId()).orElse(null);
					Division divisions = null;
					Branch branch = null;
					if (employeeDetail != null) {
						divisions = divisionRepository.findById(employeeDetail.getDivisionId()).orElse(null);
						branch = branchRepository.findById(employeeDetail.getBranchId()).orElse(null);
					}
					return dataMapper.getEmployee(employee, employeeDetail, branch, divisions);
				})
				.collect(Collectors.toList());
	}

	@Override
	public EmployeeDTO getEmployeeById(String id) throws ErrorNotFound {
		Employee employee = employeeRepository.findById(id).orElse(null);
		if (employee != null) {
			EmployeeDetail employeeDetail = employeeDetailRepository.findByEmployeeId(employee.getId()).orElse(null);
			Division divisions = null;
			Branch branch = null;
			if (employeeDetail != null) {
				divisions = divisionRepository.findById(employeeDetail.getDivisionId()).orElse(null);
				branch = branchRepository.findById(employeeDetail.getBranchId()).orElse(null);
			}
			return dataMapper.getEmployee(employee, employeeDetail, branch, divisions);
		} else {
			throw new ErrorNotFound("Data with this id is not found");
		}
	}

	@Override
	public EmployeeDTO updateEmployee(String id, RequestUpdateEmployeeDTO requestUpdateEmployeeDTO)
			throws ErrorNotFound, ErrorInvalidRequest {
		try {
			Employee employee = employeeRepository.findById(id).orElse(null);
			EmployeeDetail employeeDetail = null;
			if (employee != null) {
				employeeDetail = employeeDetailRepository.findByEmployeeId(employee.getId()).orElse(null);
				if (employeeDetail != null) {
					if (requestUpdateEmployeeDTO.getCitizenshipId() != null) {
						employeeDetail.setCitizenshipId(requestUpdateEmployeeDTO.getCitizenshipId());
					}
				}
			} else {
				throw new ErrorNotFound("Data with this id is not found");
			}
			Branch branch = null;
			if (requestUpdateEmployeeDTO.getBranchId() != null) {
				branch = branchRepository.findById(requestUpdateEmployeeDTO.getBranchId()).orElse(null);
				if (branch == null) {
					throw new ErrorNotFound("Data with this branch id is not found");
				}
				employeeDetail.setBranchId(branch.getId());
			}
			Division division = null;
			if (requestUpdateEmployeeDTO.getDivisionId() != null) {
				division = divisionRepository.findById(requestUpdateEmployeeDTO.getDivisionId()).orElse(null);
				if (division == null) {
					throw new ErrorNotFound("Data with this division id is not found");
				}
				employeeDetail.setDivisionId(division.getId());
			}
			employeeDetailRepository.save(employeeDetail);
			if (requestUpdateEmployeeDTO.getCitizenshipId() != null) {
				employeeDetail.setCitizenshipId(requestUpdateEmployeeDTO.getCitizenshipId());
			}
			if (requestUpdateEmployeeDTO.getJoinDate() != null) {
				employeeDetail.setJoinDate(DateUtils.parseDate(requestUpdateEmployeeDTO.getJoinDate()));
			}
			if (requestUpdateEmployeeDTO.getLeaveDate() != null) {
				employeeDetail.setLeaveDate(DateUtils.parseDate(requestUpdateEmployeeDTO.getLeaveDate()));
			}
			if (requestUpdateEmployeeDTO.getPosition() != null) {
				employeeDetail.setPosition(requestUpdateEmployeeDTO.getPosition());
			}
			if (requestUpdateEmployeeDTO.getName() != null) {
				employee.setName(requestUpdateEmployeeDTO.getName());
			}
			employeeDetailRepository.save(employeeDetail);
			employeeRepository.save(employee);
			if (branch == null) {
				branch = branchRepository.findById(employeeDetail.getBranchId()).orElse(null);
			}
			if (division == null) {
				division = divisionRepository.findById(employeeDetail.getDivisionId()).orElse(null);
			}
			return dataMapper.getEmployee(employee, employeeDetail, branch, division);

		} catch (Exception e) {
			throw new ErrorInvalidRequest(e.getLocalizedMessage());
		}
	}

	@Override
	public void deleteEmployee(String id) throws ErrorNotFound {
		Employee employee = employeeRepository.findById(id).orElse(null);
		if (employee != null) {
			employeeRepository.delete(employee);
		} else {
			throw new ErrorNotFound("Data with this id is not found");
		}
	}

	@Override
	public List<EmployeeDTO> findByNameLike(String name) {
		List<Employee> employees = employeeRepository.findByNameLike(name);
		return employees.stream()
				.map(employee -> {
					EmployeeDetail employeeDetail = employeeDetailRepository.findByEmployeeId(employee.getId()).orElse(null);
					Division divisions = null;
					Branch branch = null;
					if (employeeDetail != null) {
						divisions = divisionRepository.findById(employeeDetail.getDivisionId()).orElse(null);
						branch = branchRepository.findById(employeeDetail.getBranchId()).orElse(null);
					}
					return dataMapper.getEmployee(employee, employeeDetail, branch, divisions);
				})
				.collect(Collectors.toList());
	}

	@Override
	public List<EmployeeDTO> findEmployeeWhereByDivisionNameLike(String divisionName) {
		List<Employee> employees = employeeRepository.findByDivisionWhereDivisionNameLike(divisionName);
		return employees.stream()
				.map(employee -> {
					EmployeeDetail employeeDetail = employeeDetailRepository.findByEmployeeId(employee.getId()).orElse(null);
					Division divisions = null;
					Branch branch = null;
					if (employeeDetail != null) {
						divisions = divisionRepository.findById(employeeDetail.getDivisionId()).orElse(null);
						branch = branchRepository.findById(employeeDetail.getBranchId()).orElse(null);
					}
					return dataMapper.getEmployee(employee, employeeDetail, branch, divisions);
				})
				.collect(Collectors.toList());
	}

	@Override
	public List<EmployeeDTO> findEmployeeWhereByBranchNameLike(String branchName) {
		List<Employee> employees = employeeRepository.findByBranchWhereBranchNameLike(branchName);
		return employees.stream()
				.map(employee -> {
					EmployeeDetail employeeDetail = employeeDetailRepository.findByEmployeeId(employee.getId()).orElse(null);
					Division divisions = null;
					Branch branch = null;
					if (employeeDetail != null) {
						divisions = divisionRepository.findById(employeeDetail.getDivisionId()).orElse(null);
						branch = branchRepository.findById(employeeDetail.getBranchId()).orElse(null);
					}
					return dataMapper.getEmployee(employee, employeeDetail, branch, divisions);
				})
				.collect(Collectors.toList());
	}

	@Override
	public List<EmployeeDTO> findEmployeeThatAboutToLeave() {
		List<Employee> employees = employeeRepository.findEmployeeThatAboutToLeave();
		return employees.stream()
				.map(employee -> {
					EmployeeDetail employeeDetail = employeeDetailRepository.findByEmployeeId(employee.getId()).orElse(null);
					Division divisions = null;
					Branch branch = null;
					if (employeeDetail != null) {
						divisions = divisionRepository.findById(employeeDetail.getDivisionId()).orElse(null);
						branch = branchRepository.findById(employeeDetail.getBranchId()).orElse(null);
					}
					return dataMapper.getEmployee(employee, employeeDetail, branch, divisions);
				})
				.collect(Collectors.toList());
	}
}
