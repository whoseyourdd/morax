package com.whoseyourdd.morax.employee.model.redis;

import java.io.Serializable;

import org.springframework.data.redis.core.RedisHash;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@RedisHash("LoginToken")
public class LoginToken implements Serializable {
	private String token;
	private String userId;
	private String ipAddress;
	private String userAgent;
	private String loginTime;
	private String role;
	private String loginAttempt;
	private String ttl;
}
