package com.whoseyourdd.morax.employee.utils.error;

public class ErrorAlreadyExist extends Exception {

	public ErrorAlreadyExist(String message) {
		super(message);
	}
}
