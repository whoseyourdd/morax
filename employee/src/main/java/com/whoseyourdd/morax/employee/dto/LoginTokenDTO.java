package com.whoseyourdd.morax.employee.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LoginTokenDTO {
	@JsonProperty("user_id")
	private String userId;
	@JsonProperty("role")
	private String role;
}
