-- Create the common database
CREATE DATABASE IF NOT EXISTS auth;

-- Switch to the common database
USE auth;

-- Create the login table
CREATE TABLE IF NOT EXISTS login (
    id VARCHAR(255) NOT NULL UNIQUE,
    user_id VARCHAR(255) NOT NULL,
    ip_address VARCHAR(255) NOT NULL,
    user_agent VARCHAR(255) NOT NULL,
    login_time DATETIME NOT NULL,
    logout_time DATETIME,
    PRIMARY KEY (id)
);

-- Create the role table
CREATE TABLE IF NOT EXISTS role (
    id VARCHAR(255) NOT NULL UNIQUE,
    role_name VARCHAR(255) NOT NULL,
    created_at DATETIME NOT NULL,
    updated_at DATETIME,
    is_active TINYINT(1) NOT NULL,
    PRIMARY KEY (id)
);

-- Create the user table
CREATE TABLE IF NOT EXISTS user (
    id VARCHAR(255) NOT NULL UNIQUE,
    username VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL UNIQUE,
    password VARCHAR(255) NOT NULL,
    role VARCHAR(255) NOT NULL,
    password_expired VARCHAR(255) NOT NULL,
    last_login VARCHAR(255) NOT NULL,
    login_attempts INT NOT NULL,
    created_at DATETIME NOT NULL,
    updated_at DATETIME,
    is_active TINYINT(1) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (role) REFERENCES role(id) ON UPDATE CASCADE ON DELETE CASCADE
);

INSERT INTO role (id, role_name, created_at, updated_at, is_active) VALUES 
('bf6d72fe-5685-46f1-af22-6028ba166afa', 'Admin', '2024-03-10 08:00:00', NULL, 1),
('1593b741-cbb7-475c-9d57-46a655e329b1', 'User', '2024-03-10 08:00:00', NULL, 1);

INSERT INTO user (id, username, email, password, role, password_expired, last_login, login_attempts, created_at, updated_at, is_active) VALUES 
('57da78f3-67d2-4e2b-bd51-05f5e42bb163', 'admin', 'admin@example.com', '$2a$10$NMtquzli/XY030hhRx4PyeZkgTa8WIMaI6An9hHAANG90naQFTf2i', 'bf6d72fe-5685-46f1-af22-6028ba166afa'
, '2024-03-09 09:00:00', '2024-03-09 08:00:00', 0, '2024-03-09 08:00:00', NULL, 1),
('e92f17c2-f929-41d0-8b68-4a39bb6a76a5', 'user', 'user@example.com', '$2a$10$pf.SS055YurnAyYO3hrkS.LxsRDx.AbMowzIZFo9i8cz.9KonOtyK', '1593b741-cbb7-475c-9d57-46a655e329b1', '2024-03-09 09:00:00', '2024-03-09 08:00:00', 0, '2024-03-09 08:00:00', NULL, 1);


-- Create the common database
CREATE DATABASE IF NOT EXISTS common;

-- Switch to the common database
USE common;

-- Create the branches table
CREATE TABLE IF NOT EXISTS branches (
    id VARCHAR(255) NOT NULL UNIQUE,
    name VARCHAR(255) NOT NULL,
    created_at DATETIME NOT NULL,
    updated_at DATETIME,
    is_active TINYINT(1) NOT NULL,
    PRIMARY KEY (id)
);

-- Create the divisions table
CREATE TABLE IF NOT EXISTS divisions (
    id VARCHAR(255) NOT NULL UNIQUE,
    name VARCHAR(255) NOT NULL,
    created_at DATETIME NOT NULL,
    updated_at DATETIME,
    is_active TINYINT(1) NOT NULL,
    PRIMARY KEY (id)
);

-- Create the employees table
CREATE TABLE IF NOT EXISTS employees (
    id VARCHAR(255) NOT NULL UNIQUE,
    name VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL UNIQUE,
    created_at DATETIME NOT NULL,
    updated_at DATETIME,
    is_active TINYINT(1) NOT NULL,
    PRIMARY KEY (id)
);

-- Create the employee_details table
CREATE TABLE IF NOT EXISTS employee_details (
    id VARCHAR(255) NOT NULL UNIQUE,
    employee_id VARCHAR(255) NOT NULL UNIQUE,
    citizenship_id VARCHAR(255) NOT NULL UNIQUE,
    division_id VARCHAR(255) NOT NULL,
    branch_id VARCHAR(255) NOT NULL,
    position VARCHAR(255) NOT NULL,
    join_date DATETIME NOT NULL,
    leave_date DATETIME,
    created_at DATETIME NOT NULL,
    updated_at DATETIME,
    PRIMARY KEY (id),
    FOREIGN KEY (employee_id) REFERENCES employees(id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (division_id) REFERENCES divisions(id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (branch_id) REFERENCES branches(id) ON UPDATE CASCADE ON DELETE CASCADE
);

INSERT INTO branches (id, name, created_at, updated_at, is_active) VALUES 
('d998a096-59a6-41cc-b9d2-917872a3ff56', 'Branch 1', NOW(), null, 1),
('6fb7e5c1-9ee8-4c82-9e46-24097985cd1a', 'Branch 2', NOW(), null, 1),
('adac74c0-733c-4804-847f-6cb3b5b14ef1', 'Branch 3', NOW(), null, 1);

INSERT INTO divisions (id, name, created_at, updated_at, is_active) VALUES 
('b3d5b244-9b72-41a3-b8c5-152a1c5d9d3d', 'Division 1', NOW(), null, 1),
('f99d2461-3b44-41b0-86b4-9f5f4b057b9b', 'Division 2', NOW(), null, 1),
('3aa0a75e-0369-474f-9e86-97a593da787f', 'Division 3', NOW(), null, 1);

INSERT INTO employees (id, name, email, created_at, updated_at, is_active) VALUES 
('c6c5f91b-84f2-4ba5-92c5-d95e3d4776a5', 'John Doe', 'john@example.com',  NOW(), null, 1),
('a4a2b8dc-5b7c-4a7d-8905-4c84754507fc', 'Jane Doe', 'jane@example.com', NOW(), null, 1),
('32ef7f6e-6ff3-41aa-bd86-897759063eb7', 'Alice Smith', 'alice@example.com', NOW(), null, 1);

INSERT INTO employee_details (id, employee_id, citizenship_id, division_id, branch_id, position, join_date, leave_date, created_at, updated_at) VALUES 
('87a0b51a-bc30-4f21-8c5d-0f142d5f2468', 'c6c5f91b-84f2-4ba5-92c5-d95e3d4776a5', '1234567890', 'b3d5b244-9b72-41a3-b8c5-152a1c5d9d3d', 'd998a096-59a6-41cc-b9d2-917872a3ff56', 'Manager', '2024-03-09 09:00:00', null, NOW(), null),
('cf33f59d-2fd5-4714-9032-40cf7bc978c1', 'a4a2b8dc-5b7c-4a7d-8905-4c84754507fc', '0987654321', 'b3d5b244-9b72-41a3-b8c5-152a1c5d9d3d', 'd998a096-59a6-41cc-b9d2-917872a3ff56', 'Developer', '2024-03-09 09:00:00', null, NOW(), null),
('9a71f42e-4316-47d3-9b6c-f0c7cb92e636', '32ef7f6e-6ff3-41aa-bd86-897759063eb7', '1357902468', 'f99d2461-3b44-41b0-86b4-9f5f4b057b9b', '6fb7e5c1-9ee8-4c82-9e46-24097985cd1a', 'Designer', '2024-03-09 09:00:00', null, NOW(), null);

GRANT ALL PRIVILEGES ON *.* TO 'user'@'%';
FLUSH PRIVILEGES;
