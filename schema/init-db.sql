

-- ----------------------------
-- Database structure for auth
-- ----------------------------
CREATE DATABASE IF NOT EXISTS auth;
USE auth;

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;
-- ----------------------------
-- Table structure for login
-- ----------------------------
DROP TABLE IF EXISTS `login`;
CREATE TABLE `login`  (
  `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `user_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `ip_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `user_agent` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `login_time` datetime NOT NULL,
  `logout_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `id`(`id` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of login
-- ----------------------------
INSERT INTO `login` VALUES ('002e472f-eb94-4d73-87ed-54d09a608168', '57da78f3-67d2-4e2b-bd51-05f5e42bb163', '172.21.125.27', 'PostmanRuntime/7.36.3', '2024-03-11 00:02:09', NULL);
INSERT INTO `login` VALUES ('5a1a1f68-153d-4999-b514-669b5fe42233', '57da78f3-67d2-4e2b-bd51-05f5e42bb163', '172.21.125.27', 'PostmanRuntime/7.36.3', '2024-03-10 23:58:33', NULL);
INSERT INTO `login` VALUES ('d99589f6-883c-4f2e-ae11-0980684b2069', '57da78f3-67d2-4e2b-bd51-05f5e42bb163', '172.21.125.27', 'PostmanRuntime/7.36.3', '2024-03-10 23:15:34', NULL);
INSERT INTO `login` VALUES ('ee889b80-a846-4f82-9f6d-8a7c9e6a47bd', '57da78f3-67d2-4e2b-bd51-05f5e42bb163', '172.21.125.27', 'PostmanRuntime/7.36.3', '2024-03-10 23:55:19', NULL);

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role`  (
  `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `role_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NULL DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `id`(`id` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES ('1593b741-cbb7-475c-9d57-46a655e329b1', 'User', '2024-03-10 08:00:00', NULL, 1);
INSERT INTO `role` VALUES ('bf6d72fe-5685-46f1-af22-6028ba166afa', 'Admin', '2024-03-10 08:00:00', NULL, 1);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `role` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `password_expired` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `last_login` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `login_attempts` int NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NULL DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `id`(`id` ASC) USING BTREE,
  UNIQUE INDEX `email`(`email` ASC) USING BTREE,
  INDEX `role`(`role` ASC) USING BTREE,
  CONSTRAINT `user_ibfk_1` FOREIGN KEY (`role`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('57da78f3-67d2-4e2b-bd51-05f5e42bb163', 'Admin', 'admin', 'admin@example.com', '$2a$10$NMtquzli/XY030hhRx4PyeZkgTa8WIMaI6An9hHAANG90naQFTf2i', 'bf6d72fe-5685-46f1-af22-6028ba166afa', '2024-06-11 06:15:19', '2024-03-11 07:02:09', 0, '2024-03-09 08:00:00', '2024-03-10 23:15:20', 1);
INSERT INTO `user` VALUES ('e92f17c2-f929-41d0-8b68-4a39bb6a76a5', 'User', 'user', 'user@example.com', '$2a$10$pf.SS055YurnAyYO3hrkS.LxsRDx.AbMowzIZFo9i8cz.9KonOtyK', '1593b741-cbb7-475c-9d57-46a655e329b1', '2024-03-09 09:00:00', '2024-03-09 08:00:00', 0, '2024-03-09 08:00:00', NULL, 1);

SET FOREIGN_KEY_CHECKS = 1;

-- ----------------------------
-- Database structure for common
-- ----------------------------
CREATE DATABASE IF NOT EXISTS common;
USE common;

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;
-- ----------------------------
-- Table structure for branches
-- ----------------------------
DROP TABLE IF EXISTS `branches`;
CREATE TABLE `branches`  (
  `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NULL DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `id`(`id` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of branches
-- ----------------------------
INSERT INTO `branches` VALUES ('6fb7e5c1-9ee8-4c82-9e46-24097985cd1a', 'Branch 2', '2024-03-10 09:20:34', NULL, 1);
INSERT INTO `branches` VALUES ('adac74c0-733c-4804-847f-6cb3b5b14ef1', 'Branch 3', '2024-03-10 09:20:34', NULL, 1);
INSERT INTO `branches` VALUES ('d998a096-59a6-41cc-b9d2-917872a3ff56', 'Branch 1', '2024-03-10 09:20:34', NULL, 1);

-- ----------------------------
-- Table structure for divisions
-- ----------------------------
DROP TABLE IF EXISTS `divisions`;
CREATE TABLE `divisions`  (
  `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NULL DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `id`(`id` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of divisions
-- ----------------------------
INSERT INTO `divisions` VALUES ('3aa0a75e-0369-474f-9e86-97a593da787f', 'Division 3', '2024-03-10 09:20:34', NULL, 1);
INSERT INTO `divisions` VALUES ('b3d5b244-9b72-41a3-b8c5-152a1c5d9d3d', 'Division 1', '2024-03-10 09:20:34', NULL, 1);
INSERT INTO `divisions` VALUES ('f99d2461-3b44-41b0-86b4-9f5f4b057b9b', 'Division 2', '2024-03-10 09:20:34', NULL, 1);

-- ----------------------------
-- Table structure for employee_details
-- ----------------------------
DROP TABLE IF EXISTS `employee_details`;
CREATE TABLE `employee_details`  (
  `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `employee_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `citizenship_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `division_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `branch_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `position` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `join_date` datetime NOT NULL,
  `leave_date` datetime NULL DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `id`(`id` ASC) USING BTREE,
  UNIQUE INDEX `employee_id`(`employee_id` ASC) USING BTREE,
  UNIQUE INDEX `citizenship_id`(`citizenship_id` ASC) USING BTREE,
  INDEX `division_id`(`division_id` ASC) USING BTREE,
  INDEX `branch_id`(`branch_id` ASC) USING BTREE,
  CONSTRAINT `employee_details_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `employee_details_ibfk_2` FOREIGN KEY (`division_id`) REFERENCES `divisions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `employee_details_ibfk_3` FOREIGN KEY (`branch_id`) REFERENCES `branches` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of employee_details
-- ----------------------------
INSERT INTO `employee_details` VALUES ('23ea37ab-8ac2-4ef3-bd2b-25b5890387fe', 'a2bb332c-6a50-4390-8b80-6b51b2cba3c7', '12345678229', 'b3d5b244-9b72-41a3-b8c5-152a1c5d9d3d', '6fb7e5c1-9ee8-4c82-9e46-24097985cd1a', 'Software Engineer', '2021-01-01 08:00:00', '2024-12-31 01:00:00', '2024-03-10 10:13:36', NULL);
INSERT INTO `employee_details` VALUES ('87a0b51a-bc30-4f21-8c5d-0f142d5f2468', 'c6c5f91b-84f2-4ba5-92c5-d95e3d4776a5', '1234567890', 'b3d5b244-9b72-41a3-b8c5-152a1c5d9d3d', 'd998a096-59a6-41cc-b9d2-917872a3ff56', 'Manager', '2024-03-09 09:00:00', NULL, '2024-03-10 09:20:34', NULL);
INSERT INTO `employee_details` VALUES ('9a71f42e-4316-47d3-9b6c-f0c7cb92e636', '32ef7f6e-6ff3-41aa-bd86-897759063eb7', '1357902468', 'f99d2461-3b44-41b0-86b4-9f5f4b057b9b', '6fb7e5c1-9ee8-4c82-9e46-24097985cd1a', 'Designer', '2024-03-09 09:00:00', NULL, '2024-03-10 09:20:34', NULL);
INSERT INTO `employee_details` VALUES ('cf33f59d-2fd5-4714-9032-40cf7bc978c1', 'a4a2b8dc-5b7c-4a7d-8905-4c84754507fc', '0987654321', 'b3d5b244-9b72-41a3-b8c5-152a1c5d9d3d', 'd998a096-59a6-41cc-b9d2-917872a3ff56', 'Developer', '2024-03-09 09:00:00', NULL, '2024-03-10 09:20:34', NULL);

-- ----------------------------
-- Table structure for employees
-- ----------------------------
DROP TABLE IF EXISTS `employees`;
CREATE TABLE `employees`  (
  `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NULL DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `id`(`id` ASC) USING BTREE,
  UNIQUE INDEX `email`(`email` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of employees
-- ----------------------------
INSERT INTO `employees` VALUES ('32ef7f6e-6ff3-41aa-bd86-897759063eb7', 'Alice Smith', 'alice@example.com', '2024-03-10 09:20:34', NULL, 1);
INSERT INTO `employees` VALUES ('a2bb332c-6a50-4390-8b80-6b51b2cba3c7', 'Johnathan Doe', 'john.doe@example.com', '2024-03-10 10:13:36', NULL, 0);
INSERT INTO `employees` VALUES ('a4a2b8dc-5b7c-4a7d-8905-4c84754507fc', 'Jane Doe', 'jane@example.com', '2024-03-10 09:20:34', NULL, 1);
INSERT INTO `employees` VALUES ('c6c5f91b-84f2-4ba5-92c5-d95e3d4776a5', 'John Doe', 'john@example.com', '2024-03-10 09:20:34', NULL, 1);

SET FOREIGN_KEY_CHECKS = 1;

-- ----------------------------
-- User and privileges
-- ----------------------------
GRANT ALL PRIVILEGES ON *.* TO 'user'@'%';
FLUSH PRIVILEGES;
