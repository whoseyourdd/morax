#!/bin/bash

declare dc_infra=docker-compose.yml
declare dc_server=docker-compose-sv.yml
declare dc_app=docker-compose-ms.yml

function init(){
    echo "Initializing docker configuration...."
    docker network create morax --driver=bridge
    mkdir .docker
    mkdir .docker/init-db
    cp schema/init-db.sql .docker/init-db
}

function build_project() {
    cd "$1" && ./mvnw package -DskipTests && cd ..
}

function clean_project() {
    cd "$1" && ./mvnw clean && cd ..
}

function build_all(){
    build_project "employee"
    build_project "auth"
    build_project "configapp"
    build_project "discoveryserver"
    build_project "gateway"
}

function clean_all(){
    clean_project "employee"
    clean_project "auth"
    clean_project "configapp"
    clean_project "discoveryserver"
    clean_project "gateway"
}

function start() {
    build_all
    echo "Starting all docker containers...."
    docker-compose -f ${dc_infra} -f ${dc_server} -f ${dc_app} up --build -d
    
    echo "Application started...."
}

function restart() {
    echo "Restarting all docker containers (This action will not rebuild the jar)...."
   
    echo "Stopping all docker containers...."
    docker-compose -f ${dc_infra} -f ${dc_server} -f ${dc_app} stop
    docker-compose -f ${dc_infra} -f ${dc_server} -f ${dc_app} rm -f
   
    echo "Starting all docker containers...."
    docker-compose -f ${dc_infra} -f ${dc_server} -f ${dc_app} up --build -d

    echo "Application started...."
}

function stop() {
    echo "Stopping all docker containers...."
    docker-compose -f ${dc_infra} -f ${dc_server} -f ${dc_app} stop
    docker-compose -f ${dc_infra} -f ${dc_server} -f ${dc_app} rm -f
}

function start-infra() {
    echo "Starting infra docker containers...."
    docker-compose -f ${dc_infra} up --remove-orphans -d
}

function stop-infra() {
    echo "Stopping infra docker containers...."
    docker-compose -f ${dc_infra} stop
    docker-compose -f ${dc_infra} rm -f
}

function cleanup(){
    clean_all
    docker image rm project-morax-employee-ms
    docker rmi $(docker images -f "dangling=true" -q)
		docker container rm mysql-morax
		docker container rm redis-morax
		docker container rm mongodb-morax
		sudo rm -rf .docker
}

action="start"

if [[ "$#" != "0"  ]]
then
    action=$@
fi

eval ${action}
