# MORAX

MORAX is an microservices for management of employees, divisions, branches, roles, and user login functionalities.

## Features

- **Employee Management:** Efficiently add, update, and delete employee records with ease.
- **User Management:** Seamlessly manage user accounts and permissions for enhanced control.
- **User Login:** Authenticate users securely and manage login sessions effectively.

## Technologies Used

- **Java 17**
- **Spring Boot:** A Java backend framework facilitating rapid development of Java applications. The dependencies I use for this stacks: Eureka for server discovery, Config for SpringBoot Config Servers, and Gateway for API-Gateway
- **MySQL:** Utilized as the primary relational database management system for storing employee and user data securely.
- **Redis:** Employed as an in-memory data structure store for efficient caching of user sessions, enhancing application responsiveness.
- **Docker:** Leveraged for containerization, enabling seamless deployment and scaling of the application across various environments.


## How To Run

1. Initialize the docker configs:
```
    $ chmod +x start.sh
    $ ./start.sh init
```
2. Start the stacks:
```
    $ ./start.sh start
```
3. Stop the Apps:
```
    $ ./start.sh stop
```
4. Cleanup the docker environment
```
    $ ./start.sh cleanup
```
5. To restart the docker without build the projects:
```
    $ ./start.sh start-infra

    to stop:

    $ ./start.sh stop-infra

```
6. Start the infrastructure only
```
    $ ./start.sh start-infra
```
## API Collection

- **Postman Collection:** Postman collection can be found on postman folders

## Future Tasks

- **Code Refactoring:** Enhance codebase by cleaning redundant or outdated code segments and optimizing performance.
- **Endpoint Management:** Streamline and organize endpoint prefixes to improve clarity and maintainability.
- **Add Services:** Add services to manage payroll, user attendance, and report.

### Additional Tech Stacks (Future Expansion)

- **Kafka:** Introduce Kafka for enabling message streaming and asynchronous task handling between microservices. This enhancement will require further research and implementation. Further research is needed to understand Kafka's architecture, integration, and best practices for application development.
- **Elasticsearch:** Incorporate Elasticsearch to handle non-SQL databases, providing more flexibility in data storage and retrieval. Extensive research and integration efforts will be required due to the shift from traditional SQL databases Since I am more familiar with MongoDB, additional research and learning will be required to effectively leverage Elasticsearch's features and integrate it into the application architecture.
- **Kubernetes:** Adopting Kubernetes for container orchestration will enhance scalability, reliability, and deployment automation. Kubernetes provides a platform for managing containerized applications across clusters of hosts. Further exploration is necessary to understand Kubernetes concepts, such as pods, services, and deployments, and their application in deploying and managing microservices-based applications.


Feel free to reach out for further discussions or contributions to the project's development. Your feedback and collaboration are highly appreciated.
