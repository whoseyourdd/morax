package com.whoseyourdd.morax.auth.utils.error;

public class ErrorInvalidCreds extends Exception {

	public ErrorInvalidCreds(String message) {
		super(message);
	}
}
