package com.whoseyourdd.morax.auth.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.whoseyourdd.morax.auth.dto.RoleDTO;
import com.whoseyourdd.morax.auth.service.IAuthService;
import com.whoseyourdd.morax.auth.service.IRoleService;
import com.whoseyourdd.morax.auth.utils.mapper.ErrorResponseBuilder;
import com.whoseyourdd.morax.auth.utils.mapper.ResponseBuilder;

import jakarta.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/api/auth")
public class RoleController {

	@Autowired
	IRoleService roleService;

	@Autowired
	IAuthService authService;

	@Autowired
	ResponseBuilder responseBuilder;

	@Autowired
	ErrorResponseBuilder errorBuilder;

	@GetMapping("/roles")
	public ResponseEntity<?> getRoles(HttpServletRequest request) {
		Long startTime = System.currentTimeMillis();
		try {
			authService.authorizationInterceptor(request, true);
			List<RoleDTO> roles = roleService.findAll();
			return responseBuilder.buildSuccessResponse(roles, HttpStatus.OK, "Fetched roles", startTime);
		} catch (Exception e) {
			return errorBuilder.handleException(e, startTime);
		}
	}

	@PostMapping("/roles")
	public ResponseEntity<?> addRole(@RequestParam("role_name") String roleName, HttpServletRequest request) {
		Long startTime = System.currentTimeMillis();
		try {
			authService.authorizationInterceptor(request, true);
			roleService.create(roleName);
			return responseBuilder.buildSuccessResponse(null, HttpStatus.OK, "Created role", startTime);
		} catch (Exception e) {
			return errorBuilder.handleException(e, startTime);
		}
	}

	@PutMapping("/roles")
	public ResponseEntity<?> changeRoleName(@RequestParam("id") String id, @RequestParam("role_name") String roleName,
			HttpServletRequest request) {
		Long startTime = System.currentTimeMillis();
		try {
			authService.authorizationInterceptor(request, true);
			roleService.changeRoleName(id, roleName);
			return responseBuilder.buildSuccessResponse(null, HttpStatus.OK, "Updated role", startTime);
		} catch (Exception e) {
			return errorBuilder.handleException(e, startTime);
		}
	}

	@DeleteMapping("/roles")
	public ResponseEntity<?> deleteRole(@RequestParam("id") String id, HttpServletRequest request) {
		Long startTime = System.currentTimeMillis();
		try {
			authService.authorizationInterceptor(request, true);
			roleService.delete(id);
			return responseBuilder.buildSuccessResponse(null, HttpStatus.OK, "Deleted role", startTime);
		} catch (Exception e) {
			return errorBuilder.handleException(e, startTime);
		}
	}

}
