package com.whoseyourdd.morax.auth.repository.redis;

import com.whoseyourdd.morax.auth.model.redis.UserVerification;

public interface IUserVerificationRepository {

	void put(UserVerification userVerification);

	UserVerification get(String verificationCode);

	void delete(String verificationCode);
}
