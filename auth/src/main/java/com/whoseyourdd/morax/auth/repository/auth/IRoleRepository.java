package com.whoseyourdd.morax.auth.repository.auth;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.whoseyourdd.morax.auth.model.auth.Role;

public interface IRoleRepository extends JpaRepository<Role, String> {

	@Query(value = "SELECT r.id FROM role r WHERE r.role_name = ?1", nativeQuery = true)
	String getRoleIdByRoleName(String roleName);

	@Query(value = "SELECT COUNT(r.id) FROM role r WHERE r.role_name = ?1", nativeQuery = true)
	int isRoleExist(String roleName);

	@Query(value = "SELECT r.* FROM role r WHERE UPPER(r.role_name) = UPPER(:roleName) AND r.id = :roleId", nativeQuery = true)
	Optional<Role> findByRoleIdAndRoleNameIgnoreCase(@Param("roleId") String roleId, @Param("roleName") String roleName);
}
