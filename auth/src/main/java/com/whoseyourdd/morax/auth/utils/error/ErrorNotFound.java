package com.whoseyourdd.morax.auth.utils.error;

public class ErrorNotFound extends Exception {

	public ErrorNotFound(String message) {
		super(message);
	}
}
