package com.whoseyourdd.morax.auth.utils.constant;

public class MyConstant {

	// Redis Constant
	public static final String LOGIN_TOKEN_KEY = "LoginToken";
	public static final String USER_VERIFICATION_KEY = "UserVerification";

	// Request Constant
	public static final String AUTH_HEADER = "Authorization";
	public static final String AUTH_HEADER_PREFIX = "Bearer ";
}
