package com.whoseyourdd.morax.auth.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RoleDTO {

	@JsonProperty("role_id")
	private String id;

	@JsonProperty("role_name")
	private String roleName;
}
