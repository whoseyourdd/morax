package com.whoseyourdd.morax.auth.model.mongo;

import com.whoseyourdd.morax.auth.model.auth.User;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LoginToken {

	private String token;

	private User user;
}
