package com.whoseyourdd.morax.auth.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ResponseLoginDTO {
	@JsonProperty("token")
	private String token;

	@JsonProperty("token_expire")
	private String expiredAt;

	@JsonProperty("user_id")
	private String userId;

	@JsonProperty("role")
	private String role;
}
