package com.whoseyourdd.morax.auth.model.auth;

import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "login")
public class Login {

	@Id
	@Column(name = "id", nullable = false, unique = true)
	private String id;

	@Column(name = "user_id", nullable = false)
	private String userId;

	@Column(name = "ip_address", nullable = false)
	private String ipAddress;

	@Column(name = "user_agent", nullable = false)
	private String userAgent;

	@Column(name = "login_time", nullable = false)
	private Date loginTime;

	@Column(name = "logout_time")
	private Date logoutTime;
}
