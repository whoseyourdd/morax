package com.whoseyourdd.morax.auth.utils.error;

public class ErrorInvalidRequest extends Exception {

	public ErrorInvalidRequest(String message) {
		super(message);
	}
}
