package com.whoseyourdd.morax.auth.service.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.whoseyourdd.morax.auth.dto.RequestRegisterDTO;
import com.whoseyourdd.morax.auth.dto.RequestResetPasswordDTO;
import com.whoseyourdd.morax.auth.dto.RequestUserActivationDTO;
import com.whoseyourdd.morax.auth.dto.ResponseLoginDTO;
import com.whoseyourdd.morax.auth.model.auth.Login;
import com.whoseyourdd.morax.auth.model.auth.Role;
import com.whoseyourdd.morax.auth.model.auth.User;
import com.whoseyourdd.morax.auth.model.redis.LoginToken;
import com.whoseyourdd.morax.auth.model.redis.UserVerification;
import com.whoseyourdd.morax.auth.repository.auth.ILoginRepository;
import com.whoseyourdd.morax.auth.repository.auth.IRoleRepository;
import com.whoseyourdd.morax.auth.repository.auth.IUserRepository;
import com.whoseyourdd.morax.auth.repository.redis.ILoginTokenRepository;
import com.whoseyourdd.morax.auth.repository.redis.IUserVerificationRepository;
import com.whoseyourdd.morax.auth.service.IAuthService;
import com.whoseyourdd.morax.auth.utils.constant.MyConstant;
import com.whoseyourdd.morax.auth.utils.date.DateUtils;
import com.whoseyourdd.morax.auth.utils.encryptor.Base64Encryptor;
import com.whoseyourdd.morax.auth.utils.error.ErrorAlreadyExist;
import com.whoseyourdd.morax.auth.utils.error.ErrorInvalidCreds;
import com.whoseyourdd.morax.auth.utils.error.ErrorInvalidRequest;
import com.whoseyourdd.morax.auth.utils.error.ErrorMaxAttemptReached;
import com.whoseyourdd.morax.auth.utils.error.ErrorUnauthorized;
import com.whoseyourdd.morax.auth.utils.mapper.DataMapper;
import com.whoseyourdd.morax.auth.utils.password.PasswordUtils;
import com.whoseyourdd.morax.auth.utils.strings.StringsUtils;

import jakarta.servlet.http.HttpServletRequest;

@Service
public class AuthServiceImpl implements IAuthService {

	@Autowired
	DataMapper dataMapper;

	@Autowired
	IUserRepository userRepository;

	@Autowired
	ILoginRepository loginRepository;

	@Autowired
	IRoleRepository roleRepository;

	@Autowired
	ILoginTokenRepository loginTokenRepository;

	@Autowired
	IUserVerificationRepository userVerificationRepository;

	@Override
	public void register(RequestRegisterDTO requestRegisterDTO) throws ErrorAlreadyExist, ErrorInvalidRequest {
		User user = userRepository.findByEmail(requestRegisterDTO.getEmail()).orElse(null);
		if (user != null) {
			throw new ErrorAlreadyExist("Email already exists");
		}

		if (!PasswordUtils.validateInputPassword(requestRegisterDTO.getPassword())) {
			throw new ErrorInvalidRequest("Password does not meet the requirements");
		}
		if (StringsUtils.checkWhiteSpace(requestRegisterDTO.getUsername())) {
			throw new ErrorInvalidRequest("Username cannot contain whitespaces");
		}
		if (StringsUtils.isEmailValid(requestRegisterDTO.getEmail())) {
			throw new ErrorInvalidRequest("Email is not valid");
		}
		String roleId = roleRepository.getRoleIdByRoleName(requestRegisterDTO.getRole());
		if (roleId == null) {
			throw new ErrorInvalidRequest("Role does not exist");
		}

		user = userRepository.findByUsername(requestRegisterDTO.getUsername()).orElse(null);
		if (user != null) {
			throw new ErrorAlreadyExist("Username already exists");
		}

		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MONTH, 3);
		Date expirationDate = calendar.getTime();
		String passwordExpired = DateUtils.formatDate(expirationDate);

		String hashedPassword = PasswordUtils.hashPassword(requestRegisterDTO.getPassword());
		requestRegisterDTO.setPassword(hashedPassword);
		user = dataMapper.createUser(requestRegisterDTO);
		user.setPasswordExpired(passwordExpired);
		user.setRole(roleId);

		userRepository.save(user);
	}

	@Override
	public ResponseLoginDTO login(String basicAuthString, String userAgent, String ipAddress)
			throws ErrorInvalidCreds, ErrorMaxAttemptReached, ErrorInvalidRequest {

		String[] credentials = Base64Encryptor.decryptBasicAuthString(basicAuthString);
		String username = credentials[0];
		String password = credentials[1];

		User user = userRepository.findByUsername(username).orElse(null);
		if (user == null) {
			throw new ErrorInvalidCreds("Invalid Credentials");
		}

		if (!PasswordUtils.validatePassword(password, user.getPassword())) {
			throw new ErrorInvalidCreds("Invalid Credentials");
		}

		if (!user.getIsActive()) {
			throw new ErrorInvalidCreds("Your account is not active");
		}

		if (!DateUtils.isDateGreaterThanToday(user.getPasswordExpired())) {
			throw new ErrorInvalidCreds("Your password has expired");
		}

		user.setLoginAttempts(0);
		user.setLastLogin(DateUtils.formatDate(new Date()));
		userRepository.save(user);

		String id = UUID.randomUUID().toString();

		Login login = Login.builder()
				.id(id)
				.userId(user.getId())
				.userAgent(userAgent)
				.ipAddress(ipAddress)
				.loginTime(new Date())
				.build();
		loginRepository.save(login);

		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MINUTE, 60);
		Date expirationDate = calendar.getTime();
		String ttl = DateUtils.formatDate(expirationDate);

		Role role = roleRepository.findById(user.getRole()).orElse(null);
		if (role == null) {
			throw new ErrorInvalidRequest("Role does not exist");
		}
		user.setRole(role.getRoleName());

		LoginToken loginToken = dataMapper.loginTokenBuilder(user, login, ttl);
		loginTokenRepository.put(loginToken);

		return ResponseLoginDTO.builder()
				.token(loginToken.getToken())
				.expiredAt(loginToken.getTtl())
				.userId(user.getId())
				.role(role.getRoleName())
				.build();
	}

	@Override
	public void logoutUser(String token) {
		loginTokenRepository.delete(token);
	}

	@Override
	public boolean authenticateToken(String requestTokenValidateDTO)
			throws ErrorUnauthorized, ErrorInvalidCreds {
		LoginToken loginToken = (LoginToken) loginTokenRepository.get(requestTokenValidateDTO);
		if (loginToken == null) {
			throw new ErrorUnauthorized("Invalid Token");
		}
		if (!DateUtils.isDateGreaterThanToday(loginToken.getTtl())) {
			throw new ErrorInvalidCreds("Token already expired");
		}
		return true;
	}

	@Override
	public void resetPassword(String basicAuthString, RequestResetPasswordDTO requestResetPasswordDTO)
			throws ErrorInvalidCreds, ErrorInvalidRequest {

		String[] credentials = Base64Encryptor.decryptBasicAuthString(basicAuthString);
		String username = credentials[0];
		String password = credentials[1];

		User user = userRepository.findByUsername(username).orElse(null);
		if (user == null) {
			throw new ErrorInvalidCreds("Invalid Credentials");
		}

		if (!PasswordUtils.validatePassword(password, user.getPassword())) {
			throw new ErrorInvalidCreds("Invalid Credentials");
		}

		if (!PasswordUtils.validateInputPassword(requestResetPasswordDTO.getNewPassword())) {
			throw new ErrorInvalidRequest("Password does not meet the requirements");
		}

		String newPassword = PasswordUtils.hashPassword(requestResetPasswordDTO.getNewPassword());

		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MONTH, 3);
		Date expirationDate = calendar.getTime();
		String passwordExpired = DateUtils.formatDate(expirationDate);

		user.setLoginAttempts(0);
		user.setLastLogin(DateUtils.formatDate(new Date()));
		user.setPassword(newPassword);
		user.setUpdatedAt(new Date());
		user.setPasswordExpired(passwordExpired);

		userRepository.save(user);
	}

	@Override
	public RequestUserActivationDTO requestEmailVerification(String basicAuthString)
			throws ErrorInvalidCreds, ErrorUnauthorized, ErrorInvalidRequest {

		String[] credentials = Base64Encryptor.decryptBasicAuthString(basicAuthString);
		String username = credentials[0];
		String password = credentials[1];

		User user = userRepository.findByUsername(username).orElse(null);
		if (user == null) {
			throw new ErrorInvalidCreds("Invalid Credentials");
		}

		if (user.getIsActive()) {
			throw new ErrorInvalidRequest("Your account is already active");
		}

		if (!PasswordUtils.validatePassword(password, user.getPassword())) {
			throw new ErrorInvalidCreds("Invalid Credentials");
		}

		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MINUTE, 15);
		Date expirationDate = calendar.getTime();
		String passwordExpired = DateUtils.formatDate(expirationDate);

		String verificationCode = UUID.randomUUID().toString();
		UserVerification userVerification = UserVerification.builder()
				.verificationCode(verificationCode)
				.ttl(passwordExpired)
				.email(user.getEmail())
				.build();

		// For future work: send email
		userVerificationRepository.put(userVerification);
		return RequestUserActivationDTO.builder()
				.verificationCode(verificationCode)
				.build();
	}

	@Override
	public void activateUser(RequestUserActivationDTO userActivationDTO)
			throws ErrorUnauthorized, ErrorInvalidCreds, ErrorInvalidRequest {

		UserVerification userVerification = (UserVerification) userVerificationRepository
				.get(userActivationDTO.getVerificationCode());
		if (userVerification == null) {
			throw new ErrorUnauthorized("Invalid Token Verification Code");
		}
		if (!DateUtils.isDateGreaterThanToday(userVerification.getTtl())) {
			throw new ErrorInvalidCreds("Verification Code already expired");
		}
		User user = userRepository.findByEmail(userVerification.getEmail()).orElse(null);
		if (user == null) {
			throw new ErrorInvalidRequest("Email not found");
		}

		user.setIsActive(true);
		user.setUpdatedAt(new Date());
		userRepository.save(user);
		userVerificationRepository.delete(userVerification.getVerificationCode());
	}

	@Override
	public void authorizationInterceptor(HttpServletRequest request, boolean isAdminRequired)
			throws ErrorUnauthorized, ErrorInvalidRequest, ErrorInvalidCreds {
		String authorization = request.getHeader(MyConstant.AUTH_HEADER);
		if (authorization == null || !authorization.startsWith(MyConstant.AUTH_HEADER_PREFIX)) {
			throw new ErrorInvalidRequest("Invalid Authorization Header");
		}
		String[] parts = authorization.split(" ");
		if (parts.length != 2) {
			throw new ErrorInvalidCreds("Invalid Authorization Format");
		}
		String token = parts[1];
		LoginToken loginToken = (LoginToken) loginTokenRepository.get(token);
		if (loginToken == null) {
			throw new ErrorUnauthorized("Invalid Token");
		}
		String role = loginToken.getRole();
		if (!role.equalsIgnoreCase("admin")) {
			throw new ErrorUnauthorized("You don't have permission to do this action");
		}
	}
}
