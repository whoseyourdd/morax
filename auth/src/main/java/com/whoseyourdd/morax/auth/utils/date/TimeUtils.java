package com.whoseyourdd.morax.auth.utils.date;

public class TimeUtils {

	public static String durationFormatter(Long duration) {
		int seconds = (int) ((duration % 60000) / 1000);
		int millis = (int) (duration % 1000);
		return String.format("%02d.%03ds", seconds, millis);
	}
}
