package com.whoseyourdd.morax.auth.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseDTO<T> {
	@JsonProperty("status_code")
	private int statusCode;

	@JsonProperty("message")
	private String message;

	@JsonProperty("time")
	private String time;

	@JsonProperty("duration")
	private String duration;

	@JsonProperty("data")
	private T data;
}
