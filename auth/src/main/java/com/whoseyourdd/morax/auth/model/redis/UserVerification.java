package com.whoseyourdd.morax.auth.model.redis;

import org.springframework.data.redis.core.RedisHash;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@RedisHash("UserVerification")
public class UserVerification {
	private String verificationCode;
	private String ttl;
	private String email;
}
