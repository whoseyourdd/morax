package com.whoseyourdd.morax.auth.repository.redis;

import com.whoseyourdd.morax.auth.model.redis.LoginToken;
import com.whoseyourdd.morax.auth.utils.error.ErrorUnauthorized;

public interface ILoginTokenRepository {

	void put(LoginToken token);

	LoginToken get(String token) throws ErrorUnauthorized;

	void delete(String token);
}
