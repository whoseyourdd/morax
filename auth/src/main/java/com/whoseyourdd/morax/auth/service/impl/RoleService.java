package com.whoseyourdd.morax.auth.service.impl;

import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.whoseyourdd.morax.auth.dto.RoleDTO;
import com.whoseyourdd.morax.auth.model.auth.Role;
import com.whoseyourdd.morax.auth.repository.auth.IRoleRepository;
import com.whoseyourdd.morax.auth.service.IRoleService;
import com.whoseyourdd.morax.auth.utils.error.ErrorAlreadyExist;
import com.whoseyourdd.morax.auth.utils.error.ErrorUnauthorized;

@Service
public class RoleService implements IRoleService {

	@Autowired
	IRoleRepository roleRepository;

	@Override
	public List<RoleDTO> findAll() {
		List<Role> roles = roleRepository.findAll();
		return roles.stream()
				.map(role -> {
					RoleDTO roleDTO = RoleDTO.builder().id(role.getId()).roleName(role.getRoleName()).build();
					return roleDTO;
				}).collect(Collectors.toList());

	}

	@Override
	public void create(String roleName) throws ErrorAlreadyExist {
		int count = roleRepository.isRoleExist(roleName);
		if (count > 0) {
			throw new ErrorAlreadyExist("Role already exist");
		}
		String uid = UUID.randomUUID().toString();
		Role role = Role.builder().id(uid).roleName(roleName).createdAt(new Date()).isActive(true).build();
		roleRepository.save(role);
	}

	@Override
	public void delete(String roleId) throws ErrorUnauthorized {
		if (roleId == null) {
			throw new ErrorUnauthorized("Role not found");
		}
		roleRepository.deleteById(roleId);
	}

	@Override
	public void changeRoleName(String roleId, String roleName) throws ErrorUnauthorized, ErrorAlreadyExist {
		Role role = roleRepository.findById(roleId).get();
		if (role == null) {
			throw new ErrorUnauthorized("Role not found");
		}
		int count = roleRepository.isRoleExist(roleName);
		if (count > 0) {
			throw new ErrorAlreadyExist("Role already exist");
		}
		role.setRoleName(roleName);
		role.setUpdatedAt(new Date());
		roleRepository.save(role);
	}

	@Override
	public String getRoleName(String roleId) throws ErrorUnauthorized {
		Role role = roleRepository.findById(roleId).get();
		if (role == null) {
			throw new ErrorUnauthorized("Role not found");
		}
		return role.getRoleName();
	}

}
