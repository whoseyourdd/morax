package com.whoseyourdd.morax.auth.model.auth;

import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * We can add role permission in the next phase
 * Using the permission to mongo,
 * to check if user has the right permission to do specific task
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "role")
public class Role {

	@Id
	@Column(name = "id", nullable = false, unique = true)
	private String id;

	@Column(name = "role_name", nullable = false)
	private String roleName;

	@Column(name = "created_at", nullable = false)
	private Date createdAt;

	@Column(name = "updated_at")
	private Date updatedAt;

	@Column(name = "is_active", nullable = false)
	private Boolean isActive;
}
