package com.whoseyourdd.morax.auth.utils.error;

public class ErrorUnauthorized extends Exception {

	public ErrorUnauthorized(String message) {
		super(message);
	}
}
