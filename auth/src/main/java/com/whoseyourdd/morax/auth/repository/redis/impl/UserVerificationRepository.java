package com.whoseyourdd.morax.auth.repository.redis.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import com.whoseyourdd.morax.auth.model.redis.UserVerification;
import com.whoseyourdd.morax.auth.repository.redis.IUserVerificationRepository;
import com.whoseyourdd.morax.auth.utils.constant.MyConstant;

@Repository
public class UserVerificationRepository implements IUserVerificationRepository {

	@Autowired
	RedisTemplate<String, Object> redisTemplate;

	@Override
	public void put(UserVerification userVerificationCode) {
		redisTemplate.opsForHash().put(MyConstant.USER_VERIFICATION_KEY, userVerificationCode.getVerificationCode(),
				userVerificationCode);
	}

	@Override
	public UserVerification get(String userVerificationCode) {
		return (UserVerification) redisTemplate.opsForHash().get(MyConstant.USER_VERIFICATION_KEY, userVerificationCode);
	}

	@Override
	public void delete(String userVerificationCode) {
		redisTemplate.opsForHash().delete(MyConstant.USER_VERIFICATION_KEY, userVerificationCode);
	}
}
