package com.whoseyourdd.morax.auth.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.whoseyourdd.morax.auth.dto.RequestRegisterDTO;
import com.whoseyourdd.morax.auth.dto.RequestResetPasswordDTO;
import com.whoseyourdd.morax.auth.dto.RequestTokenValidationDTO;
import com.whoseyourdd.morax.auth.dto.RequestUserActivationDTO;
import com.whoseyourdd.morax.auth.dto.ResponseLoginDTO;
import com.whoseyourdd.morax.auth.service.IAuthService;
import com.whoseyourdd.morax.auth.utils.mapper.ErrorResponseBuilder;
import com.whoseyourdd.morax.auth.utils.mapper.ResponseBuilder;

import jakarta.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/api/auth")
public class AuthController {

	@Autowired
	IAuthService authService;

	@Autowired
	ResponseBuilder responseBuilder;

	@Autowired
	ErrorResponseBuilder errorBuilder;

	@PostMapping("/login")
	public ResponseEntity<?> login(HttpServletRequest request) {
		Long startTime = System.currentTimeMillis();
		try {
			String ipAddress = request.getHeader("X-Remote-Address");
			String userAgent = request.getHeader("User-Agent");
			String authorization = request.getHeader("Authorization");
			ResponseLoginDTO token = authService.login(authorization, userAgent, ipAddress);
			return responseBuilder.buildSuccessResponse(token, HttpStatus.OK, "Login success", startTime);
		} catch (Exception e) {
			return errorBuilder.handleException(e, startTime);
		}
	}

	@PostMapping("/logout")
	public ResponseEntity<?> logout(@RequestBody RequestTokenValidationDTO token) {
		Long startTime = System.currentTimeMillis();
		try {
			authService.logoutUser(token.getToken());
			return responseBuilder.buildSuccessResponse(null, HttpStatus.OK, "Logout success", startTime);
		} catch (Exception e) {
			return errorBuilder.handleException(e, startTime);
		}
	}

	@PostMapping("/validate")
	public ResponseEntity<?> authenticateToken(@RequestParam("token") String token) {
		Long startTime = System.currentTimeMillis();
		try {
			authService.authenticateToken(token);
			return responseBuilder.buildSuccessResponse(null, HttpStatus.OK, "Token is valid", startTime);
		} catch (Exception e) {
			return errorBuilder.handleException(e, startTime);
		}
	}

	@PostMapping("/register")
	public ResponseEntity<?> register(@RequestBody RequestRegisterDTO requestRegisterDTO, HttpServletRequest request) {
		Long startTime = System.currentTimeMillis();
		try {
			authService.authorizationInterceptor(request, true);
			authService.register(requestRegisterDTO);
			return responseBuilder.buildSuccessResponse(null, HttpStatus.CREATED, "Register success", startTime);
		} catch (Exception e) {
			return errorBuilder.handleException(e, startTime);
		}
	}

	@PostMapping("/reset-password")
	public ResponseEntity<?> resetPassword(@RequestBody RequestResetPasswordDTO requestResetPasswordDTO,
			HttpServletRequest request) {
		Long startTime = System.currentTimeMillis();
		try {
			String basicAuthString = request.getHeader("Authorization");
			authService.resetPassword(basicAuthString, requestResetPasswordDTO);
			return responseBuilder.buildSuccessResponse(null, HttpStatus.OK, "Reset password success", startTime);
		} catch (Exception e) {
			return errorBuilder.handleException(e, startTime);
		}
	}

	@PostMapping("/request-activation")
	public ResponseEntity<?> requestActivation(HttpServletRequest request) {
		Long startTime = System.currentTimeMillis();
		try {
			String basicAuthString = request.getHeader("Authorization");
			RequestUserActivationDTO response = authService.requestEmailVerification(basicAuthString);
			return responseBuilder.buildSuccessResponse(response, HttpStatus.OK, "Request activation sent", startTime);
		} catch (Exception e) {
			return errorBuilder.handleException(e, startTime);
		}
	}

	@PostMapping("/activate-user")
	public ResponseEntity<?> activateUser(@RequestBody RequestUserActivationDTO requestUserActivationDTO) {
		Long startTime = System.currentTimeMillis();
		try {
			authService.activateUser(requestUserActivationDTO);
			return responseBuilder.buildSuccessResponse(null, HttpStatus.OK, "User activation success", startTime);
		} catch (Exception e) {
			return errorBuilder.handleException(e, startTime);
		}
	}
}
