package com.whoseyourdd.morax.auth.repository.auth;

import org.springframework.data.jpa.repository.JpaRepository;

import com.whoseyourdd.morax.auth.model.auth.Login;

public interface ILoginRepository extends JpaRepository<Login, String> {

}
