package com.whoseyourdd.morax.auth.utils.password;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class PasswordUtils {

	private static final String PASSWORD_PATTERN = "^(?=.*[!@#$%^&+=])(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9]).{8,64}$";

	public static Boolean validateInputPassword(String password) {
		Pattern pattern = Pattern.compile(PASSWORD_PATTERN);
		Matcher matcher = pattern.matcher(password);
		return matcher.matches();
	}

	private static final BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

	public static String hashPassword(String password) {
		return encoder.encode(password);
	}

	public static Boolean validatePassword(String inputPassword, String password) {
		return encoder.matches(inputPassword, password);
	}
}
