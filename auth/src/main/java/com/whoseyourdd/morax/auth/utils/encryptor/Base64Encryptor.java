package com.whoseyourdd.morax.auth.utils.encryptor;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

import org.springframework.stereotype.Component;

import com.whoseyourdd.morax.auth.utils.error.ErrorInvalidRequest;

@Component
public class Base64Encryptor {

	public static String[] decryptBasicAuthString(String basicAuthString) throws ErrorInvalidRequest {
		if (basicAuthString == null || !basicAuthString.startsWith("Basic ")) {
			throw new ErrorInvalidRequest("Missing Basic Auth header");
		}

		// Split username and password
		String encodedCredentials = basicAuthString.substring(6).trim();
		String decodedCredentials = new String(Base64.getDecoder().decode(encodedCredentials), StandardCharsets.UTF_8);
		String[] credentials = decodedCredentials.split(":", 2);
		if (credentials.length != 2) {
			throw new ErrorInvalidRequest("Invalid Basic Auth string");
		}
		return credentials;
	}
}
