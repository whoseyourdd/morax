package com.whoseyourdd.morax.auth.utils.error;

public class ErrorMaxAttemptReached extends Exception {

	public ErrorMaxAttemptReached(String message) {
		super(message);
	}
}
