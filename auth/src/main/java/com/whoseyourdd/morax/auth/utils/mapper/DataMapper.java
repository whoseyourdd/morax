package com.whoseyourdd.morax.auth.utils.mapper;

import java.util.Date;
import java.util.UUID;

import org.springframework.stereotype.Component;

import com.whoseyourdd.morax.auth.dto.RequestRegisterDTO;
import com.whoseyourdd.morax.auth.model.auth.Login;
import com.whoseyourdd.morax.auth.model.auth.User;
import com.whoseyourdd.morax.auth.model.redis.LoginToken;
import com.whoseyourdd.morax.auth.utils.date.DateUtils;

@Component
public class DataMapper {

	public User createUser(RequestRegisterDTO requestRegisterDTO) {
		return User.builder()
				.id(UUID.randomUUID().toString())
				.name(requestRegisterDTO.getName())
				.username(requestRegisterDTO.getUsername())
				.email(requestRegisterDTO.getEmail())
				.password(requestRegisterDTO.getPassword())
				.loginAttempts(0)
				.lastLogin(DateUtils.formatDate(new Date()))
				.createdAt(new Date())
				.isActive(false)
				.build();
	}

	public LoginToken loginTokenBuilder(User user, Login login, String ttl) {
		return LoginToken.builder()
				.token(UUID.randomUUID().toString())
				.userId(login.getUserId())
				.ipAddress(login.getIpAddress())
				.userAgent(login.getUserAgent())
				.loginTime(DateUtils.formatDate(new Date()))
				.role(user.getRole())
				.ttl(ttl)
				.build();
	}
}
