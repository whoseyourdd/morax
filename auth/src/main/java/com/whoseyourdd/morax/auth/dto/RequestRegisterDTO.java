package com.whoseyourdd.morax.auth.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RequestRegisterDTO {
	@JsonProperty("email")
	public String email;

	@JsonProperty("password")
	public String password;

	@JsonProperty("name")
	public String name;

	@JsonProperty("username")
	public String username;

	@JsonProperty("role")
	public String role;
}
