package com.whoseyourdd.morax.auth.service;

import com.whoseyourdd.morax.auth.dto.RequestRegisterDTO;
import com.whoseyourdd.morax.auth.dto.RequestResetPasswordDTO;
import com.whoseyourdd.morax.auth.dto.RequestUserActivationDTO;
import com.whoseyourdd.morax.auth.dto.ResponseLoginDTO;
import com.whoseyourdd.morax.auth.utils.error.ErrorAlreadyExist;
import com.whoseyourdd.morax.auth.utils.error.ErrorInvalidCreds;
import com.whoseyourdd.morax.auth.utils.error.ErrorInvalidRequest;
import com.whoseyourdd.morax.auth.utils.error.ErrorMaxAttemptReached;
import com.whoseyourdd.morax.auth.utils.error.ErrorUnauthorized;

import jakarta.servlet.http.HttpServletRequest;

public interface IAuthService {

	void register(RequestRegisterDTO requestRegisterDTO)
			throws ErrorAlreadyExist, ErrorInvalidRequest;

	ResponseLoginDTO login(String basicAuthString, String userAgent, String ipAddress)
			throws ErrorInvalidCreds, ErrorMaxAttemptReached, ErrorInvalidRequest;

	void logoutUser(String token);

	void resetPassword(String basicAuthString, RequestResetPasswordDTO requestResetPasswordDTO)
			throws ErrorInvalidCreds, ErrorMaxAttemptReached, ErrorInvalidRequest;

	boolean authenticateToken(String requestTokenValidateDTO)
			throws ErrorUnauthorized, ErrorInvalidCreds;

	RequestUserActivationDTO requestEmailVerification(String basicAuthString)
			throws ErrorInvalidCreds, ErrorUnauthorized, ErrorInvalidRequest;

	void activateUser(RequestUserActivationDTO userActivationDTO)
			throws ErrorUnauthorized, ErrorInvalidCreds, ErrorInvalidRequest;

	void authorizationInterceptor(HttpServletRequest request, boolean isAdminRequired)
			throws ErrorUnauthorized, ErrorInvalidCreds, ErrorInvalidRequest;
}
