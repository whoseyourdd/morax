package com.whoseyourdd.morax.auth.utils.mapper;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.whoseyourdd.morax.auth.dto.ErrorResponseDTO;
import com.whoseyourdd.morax.auth.utils.date.DateUtils;
import com.whoseyourdd.morax.auth.utils.date.TimeUtils;
import com.whoseyourdd.morax.auth.utils.error.ErrorAlreadyExist;
import com.whoseyourdd.morax.auth.utils.error.ErrorInvalidCreds;
import com.whoseyourdd.morax.auth.utils.error.ErrorInvalidRequest;
import com.whoseyourdd.morax.auth.utils.error.ErrorMaxAttemptReached;
import com.whoseyourdd.morax.auth.utils.error.ErrorUnauthorized;

@Component
public class ErrorResponseBuilder {

	public ResponseEntity<?> handleException(Exception e, Long startTime) {
		int statusCode = HttpStatus.INTERNAL_SERVER_ERROR.value();
		if (e instanceof ErrorAlreadyExist) {
			statusCode = HttpStatus.CONFLICT.value();
		} else if (e instanceof ErrorInvalidCreds) {
			statusCode = HttpStatus.UNAUTHORIZED.value();
		} else if (e instanceof ErrorMaxAttemptReached) {
			statusCode = HttpStatus.FORBIDDEN.value();
		} else if (e instanceof ErrorInvalidRequest) {
			statusCode = HttpStatus.BAD_REQUEST.value();
		} else if (e instanceof ErrorMaxAttemptReached){
			statusCode = HttpStatus.FORBIDDEN.value();
		} else if (e instanceof ErrorUnauthorized){
			statusCode = HttpStatus.UNAUTHORIZED.value();
		}
		Long duration = System.currentTimeMillis() - startTime;
		ErrorResponseDTO responseError = ErrorResponseDTO.builder()
				.statusCode(statusCode)
				.error(e.getMessage())
				.duration(TimeUtils.durationFormatter(duration))
				.time(DateUtils.formatDate(new Date()))
				.build();
		return ResponseEntity.status(statusCode).body(responseError);
	}

	// public ResponseEntity<?> handleUnknownException(Exception e, Long startTime)
	// {
	// Long duration = System.currentTimeMillis() - startTime;
	// // e.printStackTrace();
	// ErrorResponseDTO responseError = ErrorResponseDTO.builder()
	// .statusCode(HttpStatus.INTERNAL_SERVER_ERROR.value())
	// .error(e.getMessage())
	// .duration(TimeUtils.durationFormatter(duration))
	// .time(DateUtils.formatDate(new Date()))
	// .build();
	// return
	// ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(responseError);
	// }
}
