package com.whoseyourdd.morax.auth.repository.auth;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.whoseyourdd.morax.auth.model.auth.User;

import io.lettuce.core.dynamic.annotation.Param;

public interface IUserRepository extends JpaRepository<User, String> {

	@Query(value = "SELECT * FROM user WHERE email = :email", nativeQuery = true)
	Optional<User> findByEmail(@Param("email") String email);

	@Query(value = "SELECT * FROM user WHERE username = :username", nativeQuery = true)
	Optional<User> findByUsername(@Param("username") String username);
}
