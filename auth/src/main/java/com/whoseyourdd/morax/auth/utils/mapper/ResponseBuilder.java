package com.whoseyourdd.morax.auth.utils.mapper;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.whoseyourdd.morax.auth.dto.ResponseDTO;
import com.whoseyourdd.morax.auth.utils.date.DateUtils;
import com.whoseyourdd.morax.auth.utils.date.TimeUtils;

@Component
public class ResponseBuilder {

	public <T> ResponseEntity<ResponseDTO<T>> buildSuccessResponse(T data, HttpStatus status, String message,
			Long startTime) {
		String time = DateUtils.formatDate(new Date());
		Long duration = System.currentTimeMillis() - startTime;
		ResponseDTO<T> responseDTO = ResponseDTO.<T>builder()
				.statusCode(status.value())
				.message(message)
				.data(data)
				.duration(TimeUtils.durationFormatter(duration))
				.time(time)
				.build();
		return ResponseEntity.status(status).body(responseDTO);
	}
}
