package com.whoseyourdd.morax.auth.utils.error;

public class ErrorAlreadyExist extends Exception {

	public ErrorAlreadyExist(String message) {
		super(message);
	}
}
