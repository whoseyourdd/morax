package com.whoseyourdd.morax.auth.service;

import java.util.List;

import com.whoseyourdd.morax.auth.dto.RoleDTO;
import com.whoseyourdd.morax.auth.utils.error.ErrorAlreadyExist;
import com.whoseyourdd.morax.auth.utils.error.ErrorUnauthorized;

public interface IRoleService {
	List<RoleDTO> findAll();

	void create(String roleName) throws ErrorAlreadyExist;

	void delete(String roleId) throws ErrorUnauthorized;

	void changeRoleName(String roleId, String roleName) throws ErrorUnauthorized, ErrorAlreadyExist;

	String getRoleName(String roleId) throws ErrorUnauthorized;

}
